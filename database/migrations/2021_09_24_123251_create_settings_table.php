<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('common', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('value');
            $table->timestamps();
        });

        DB::table('common')->insert(
            [
                [ 'code' => 'timeForPayment', 'name' => 'Срок для оплаты заказа клиентов (мин.)', 'value' => '30', 'created_at' => now() ],
                [ 'code' => 'timeForRefund', 'name' => 'Срок для осуществления возврата товара (мин.)', 'value' => '14', 'created_at' => now() ],
                [ 'code' => 'maxTimeOrderProcessing', 'name' => 'Максимальное время обработки заказа (мин.)', 'value' => '12', 'created_at' => now() ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('common');
    }
};
