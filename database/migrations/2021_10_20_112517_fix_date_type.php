<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Tpetry\PostgresqlEnhanced\Schema\Blueprint;
use Tpetry\PostgresqlEnhanced\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('deliveries')->whereNull('status_at')->update(['status_at' => now()]);
        DB::table('deliveries')->whereNull('delivery_at')->update(['delivery_at' => now()]);
        Schema::table('deliveries', function (Blueprint $table) {
            $table->date('delivery_at')->nullable(false)->change();
            $table->renameColumn('delivery_at', 'date');
            $table->json('timeslot')->nullable();
        });
        DB::statement('alter table deliveries alter column status_at type timestamp(6) using status_at::timestamp(6)');
        DB::statement('alter table deliveries alter column status_at set not null');
        DB::statement('alter table deliveries alter column created_at type timestamp(6) using created_at::timestamp(6)');
        DB::statement('alter table deliveries alter column updated_at type timestamp(6) using updated_at::timestamp(6)');
        DB::statement('alter table order_items alter column created_at type timestamp(6) using created_at::timestamp(6)');
        DB::statement('alter table order_items alter column updated_at type timestamp(6) using updated_at::timestamp(6)');
        DB::table('orders')->whereNull('status_at')->update(['status_at' => now()]);
        DB::table('orders')->whereNull('payment_status_at')->update(['payment_status_at' => now()]);
        DB::statement('alter table orders alter column status_at type timestamp(6) using status_at::timestamp(6)');
        DB::statement('alter table orders alter column status_at set not null');
        DB::statement('alter table orders alter column payment_status_at type timestamp(6) using payment_status_at::timestamp(6)');
        DB::statement('alter table orders alter column payment_status_at set not null');
        DB::statement('alter table orders alter column is_problem_at type timestamp(6) using is_problem_at::timestamp(6)');
        DB::statement('alter table orders alter column created_at type timestamp(6) using created_at::timestamp(6)');
        DB::statement('alter table orders alter column updated_at type timestamp(6) using updated_at::timestamp(6)');
        DB::statement('alter table orders alter column payed_at type timestamp(6) using payed_at::timestamp(6)');
        DB::statement('alter table orders alter column payment_expires_at type timestamp(6) using payment_expires_at::timestamp(6)');

        DB::statement('alter table orders_comments alter column created_at type timestamp(6) using created_at::timestamp(6)');
        DB::statement('alter table orders_comments alter column updated_at type timestamp(6) using updated_at::timestamp(6)');
        DB::table('shipments')->whereNull('status_at')->update(['status_at' => now()]);
        DB::statement('alter table shipments alter column status_at type timestamp(6) using status_at::timestamp(6)');
        DB::statement('alter table shipments alter column status_at set not null');
        DB::statement('alter table shipments alter column created_at type timestamp(6) using created_at::timestamp(6)');
        DB::statement('alter table shipments alter column updated_at type timestamp(6) using updated_at::timestamp(6)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table deliveries alter column date type timestamp(6) using date::timestamp(6)');
        DB::statement('alter table deliveries alter column date drop not null');
        DB::statement('alter table deliveries rename column date to delivery_at');
        DB::statement('alter table deliveries drop column timeslot');
        DB::statement('alter table deliveries alter column status_at type timestamp(0) using status_at::timestamp(0)');
        DB::statement('alter table deliveries alter column status_at drop not null');
        DB::statement('alter table deliveries alter column created_at type timestamp(0) using created_at::timestamp(0)');
        DB::statement('alter table deliveries alter column updated_at type timestamp(0) using updated_at::timestamp(0)');
        DB::statement('alter table order_items alter column created_at type timestamp(0) using created_at::timestamp(0)');
        DB::statement('alter table order_items alter column updated_at type timestamp(0) using updated_at::timestamp(0)');
        DB::statement('alter table orders alter column status_at type timestamp(0) using status_at::timestamp(0)');
        DB::statement('alter table orders alter column status_at drop not null');
        DB::statement('alter table orders alter column payment_status_at type timestamp(0) using payment_status_at::timestamp(0)');
        DB::statement('alter table orders alter column payment_status_at drop not null');
        DB::statement('alter table orders alter column is_problem_at type timestamp(0) using is_problem_at::timestamp(0)');
        DB::statement('alter table orders alter column created_at type timestamp(0) using created_at::timestamp(0)');
        DB::statement('alter table orders alter column updated_at type timestamp(0) using updated_at::timestamp(0)');
        DB::statement('alter table orders alter column payed_at type timestamp(0) using payed_at::timestamp(0)');
        DB::statement('alter table orders alter column payment_expires_at type timestamp(0) using payment_expires_at::timestamp(0)');
        DB::statement('alter table orders_comments alter column created_at type timestamp(0) using created_at::timestamp(0)');
        DB::statement('alter table orders_comments alter column updated_at type timestamp(0) using updated_at::timestamp(0)');
        DB::statement('alter table shipments alter column status_at type timestamp(0) using status_at::timestamp(0)');
        DB::statement('alter table shipments alter column status_at drop not null');
        DB::statement('alter table shipments alter column created_at type timestamp(0) using created_at::timestamp(0)');
        DB::statement('alter table shipments alter column updated_at type timestamp(0) using updated_at::timestamp(0)');
    }
};
