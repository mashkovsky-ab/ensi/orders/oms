<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('is_canceled');
            $table->dropColumn('is_canceled_at');
            $table->dropColumn('is_require_check');
        });
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('is_canceled');
            $table->dropColumn('is_canceled_at');
            $table->dropColumn('payment_status');
            $table->dropColumn('payment_status_at');
        });
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('is_canceled');
            $table->dropColumn('is_canceled_at');
            $table->dropColumn('payment_status');
            $table->dropColumn('payment_status_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('is_canceled')->default(false);
            $table->dateTime('is_canceled_at')->nullable();
            $table->boolean('is_require_check')->default(false);
        });
        Schema::table('deliveries', function (Blueprint $table) {
            $table->boolean('is_canceled')->default(false);
            $table->dateTime('is_canceled_at')->nullable();
            $table->unsignedTinyInteger('payment_status')->default(1);
            $table->dateTime('payment_status_at')->nullable();
        });
        Schema::table('shipments', function (Blueprint $table) {
            $table->boolean('is_canceled')->default(false);
            $table->dateTime('is_canceled_at')->nullable();
            $table->unsignedTinyInteger('payment_status')->default(1);
            $table->dateTime('payment_status_at')->nullable();
        });
    }
};
