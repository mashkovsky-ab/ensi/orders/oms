<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('price_per_one')->nullable();
            $table->unsignedBigInteger('cost_per_one')->nullable();
        });

        DB::statement('
            UPDATE order_items 
            SET 
                price_per_one = price / qty,
                cost_per_one = cost / qty
        ');

        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('price_per_one')->nullable(false)->change();
            $table->unsignedBigInteger('cost_per_one')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('price_per_one');
            $table->dropColumn('cost_per_one');
        });
    }
};
