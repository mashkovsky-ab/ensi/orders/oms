<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refunds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('manager_id')->nullable();
            $table->unsignedBigInteger('responsible_id')->nullable();
            $table->unsignedSmallInteger('source');
            $table->unsignedTinyInteger('status');
            $table->unsignedBigInteger('price');
            $table->boolean('is_partial');
            $table->text('user_comment');
            $table->text('rejection_comment')->nullable();

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
        });

        Schema::create('refund_order_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('refund_id');
            $table->unsignedBigInteger('order_item_id');
            $table->decimal('qty', 18, 4);

            $table->timestamps();

            $table->foreign('refund_id')->references('id')->on('refunds');
            $table->foreign('order_item_id')->references('id')->on('order_items');
        });

        Schema::create('refund_reasons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 20);
            $table->string('name', 255);
            $table->text('description')->nullable();

            $table->unique('code');

            $table->timestamps();
        });

        Schema::create('refund_refund_reason', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('refund_id');
            $table->unsignedBigInteger('refund_reason_id');

            $table->timestamps();

            $table->foreign('refund_id')->references('id')->on('refunds');
            $table->foreign('refund_reason_id')->references('id')->on('refund_reasons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refund_refund_reason');
        Schema::dropIfExists('refund_reasons');
        Schema::dropIfExists('refund_order_item');
        Schema::dropIfExists('refunds');
    }
};
