<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('history_main_entity');
        Schema::dropIfExists('history');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('history', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedTinyInteger('type');
            $table->jsonb('data')->nullable();

            $table->string('entity');
            $table->unsignedBigInteger('entity_id');

            $table->timestamps();
        });

        Schema::create('history_main_entity', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('history_id');
            $table->string('main_entity');
            $table->unsignedBigInteger('main_entity_id');

            $table->timestamps();

            $table->foreign('history_id')->references('id')->on('history');
        });
    }
};
