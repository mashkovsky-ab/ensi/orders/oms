<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** order_items */
        DB::statement('UPDATE order_items SET cost = cost * 100, price = price * 100');
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('price')->nullable(false)->change();
            $table->unsignedBigInteger('cost')->nullable(false)->change();
        });

        /** deliveries */
        DB::statement('UPDATE deliveries SET cost = cost * 100');
        Schema::table('deliveries', function (Blueprint $table) {
            $table->unsignedBigInteger('cost')->nullable(false)->change();
        });

        /** orders */
        DB::statement('
                UPDATE orders 
                SET 
                    cost = cost * 100,
                    price = price * 100,
                    delivery_cost = delivery_cost * 100,
                    delivery_price = delivery_price * 100
               ');

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('cost')->nullable(false)->change();
            $table->unsignedBigInteger('price')->nullable(false)->change();
            $table->unsignedBigInteger('delivery_cost')->nullable(false)->change();
            $table->unsignedBigInteger('delivery_price')->nullable(false)->change();
        });

        /** shipments */
        DB::statement('UPDATE shipments SET cost = cost * 100');
        Schema::table('shipments', function (Blueprint $table) {
            $table->unsignedBigInteger('cost')->nullable(false)->change();
        });

        /** Truncate order_discounts */
        DB::statement('TRUNCATE order_discounts');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /** basket_items */
        Schema::table('order_items', function (Blueprint $table) {
            $table->decimal('price', 18, 4)->nullable(false)->change();
            $table->decimal('cost', 18, 4)->nullable(false)->change();
        });
        DB::statement('UPDATE order_items SET cost = cost / 100, price = price / 100');

        /** deliveries */
        Schema::table('deliveries', function (Blueprint $table) {
            $table->decimal('cost', 18, 4)->default(0.0)->change();
        });
        DB::statement('UPDATE deliveries SET cost = cost / 100');

        /** orders */
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('price', 18, 4)->nullable()->change();
            $table->decimal('cost', 18, 4)->default(0.0)->change();
            $table->decimal('delivery_cost')->default(0.0)->change();
            $table->decimal('delivery_price')->default(0.0)->change();
        });

        DB::statement('
                UPDATE orders 
                SET 
                    cost = cost / 100,
                    price = price / 100,
                    delivery_cost = delivery_cost / 100,
                    delivery_price = delivery_price / 100
               ');

        /** shipments */
        Schema::table('shipments', function (Blueprint $table) {
            $table->decimal('cost', 18, 4)->default(0.0)->change();
        });
        DB::statement('UPDATE shipments SET cost = cost / 100');
    }
};
