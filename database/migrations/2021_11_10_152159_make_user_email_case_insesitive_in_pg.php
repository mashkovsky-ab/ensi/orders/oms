<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connection = config('database.default');
        $driver = config("database.connections.{$connection}.driver");

        if ($driver === 'pgsql') {
            DB::statement('CREATE EXTENSION IF NOT EXISTS citext');
            DB::statement('ALTER TABLE deliveries ALTER COLUMN number TYPE citext');
            DB::statement('ALTER TABLE order_items ALTER COLUMN name TYPE citext');
            DB::statement('ALTER TABLE orders ALTER COLUMN number TYPE citext');
            DB::statement('ALTER TABLE orders ALTER COLUMN receiver_name TYPE citext');
            DB::statement('ALTER TABLE orders ALTER COLUMN receiver_phone TYPE citext');
            DB::statement('ALTER TABLE orders ALTER COLUMN receiver_email TYPE citext');
            DB::statement('ALTER TABLE orders ALTER COLUMN promo_code TYPE citext');
            DB::statement('ALTER TABLE shipments ALTER COLUMN number TYPE citext');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
