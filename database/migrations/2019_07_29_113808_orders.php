<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class Orders
 */
return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('merchant_id');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('session_id', 255)->nullable();
            $table->unsignedInteger('type');
            $table->boolean('is_belongs_to_order')->default(false);

            $table->timestamps();
        });

        Schema::create('basket_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('basket_id')->nullable();
            $table->unsignedBigInteger('offer_id')->nullable();
            $table->unsignedInteger('type');
            $table->json('product');
            $table->string('name');
            $table->decimal('qty', 18, 4);
            $table->decimal('price', 18, 4)->nullable();
            $table->decimal('cost', 18, 4)->default(0.0);
            $table->bigInteger('referrer_id')->nullable();

            $table->timestamps();

            $table->foreign('basket_id')->references('id')->on('baskets');
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number');

            $table->unsignedBigInteger('basket_id')->unique();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('merchant_id');

            // prices & marketing
            // полная стоимость заказанных товаров
            $table->decimal('cost', 18, 4)->default(0.0);
            // итоговая стоимость заказа со всеми скидками и доставкой
            $table->decimal('price', 18, 4)->default(0.0);
            $table->decimal('delivery_price', 18, 4)->default(0.0);
            $table->decimal('delivery_cost', 18, 4)->default(0.0);

            $table->integer('spent_bonus')->default(0);
            $table->integer('added_bonus')->default(0);
            $table->json('certificates')->nullable();

            // delivery
            $table->unsignedTinyInteger('delivery_type');
            $table->text('delivery_comment')->nullable();

            // statuses
            $table->unsignedTinyInteger('status')->default(1);
            $table->dateTime('status_at')->nullable();
            $table->tinyInteger('payment_status', false, true)->default(1);
            $table->dateTime('payment_status_at')->nullable();
            $table->boolean('is_problem')->default(false);
            $table->dateTime('is_problem_at')->nullable();
            $table->boolean('is_canceled')->default(false);
            $table->dateTime('is_canceled_at')->nullable();
            $table->boolean('is_require_check')->default(false);
            // management
            $table->text('manager_comment')->nullable();

            $table->timestamps();

            $table->foreign('basket_id')->references('id')->on('baskets');
        });

        Schema::create('order_discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->json('discounts')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });

        Schema::create('order_promo_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('promo_code_id');
            $table->string('name', 255);
            $table->string('code', 255);
            $table->unsignedTinyInteger('type');
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->unsignedBigInteger('gift_id')->nullable();
            $table->unsignedBigInteger('bonus_id')->nullable();
            $table->boolean('is_personal');

            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });

        Schema::create('history', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedTinyInteger('type');
            $table->jsonb('data')->nullable();

            $table->string('entity');
            $table->unsignedBigInteger('entity_id');

            $table->timestamps();
        });

        Schema::create('history_main_entity', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('history_id');
            $table->string('main_entity');
            $table->unsignedBigInteger('main_entity_id');

            $table->timestamps();

            $table->foreign('history_id')->references('id')->on('history');
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedTinyInteger('payment_system');
            $table->unsignedTinyInteger('status')->default(1);

            $table->float('sum');
            $table->tinyInteger('payment_method', false, true);
            $table->dateTime('payed_at')->nullable();
            $table->dateTime('expires_at')->nullable();

            $table->json('data');
            $table->string('payment_link')->nullable();
            $table->string('external_id')->nullable();

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
        });

        Schema::create('delivery', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('delivery_method');
            $table->unsignedTinyInteger('delivery_service');
            $table->tinyInteger('status', false, true)->default(1);
            $table->string('status_external_id')->nullable();
            $table->unsignedTinyInteger('payment_status')->default(1);
            $table->dateTime('payment_status_at')->nullable();
            $table->boolean('is_canceled')->default(false);
            $table->dateTime('is_canceled_at')->nullable();
            $table->boolean('is_problem')->default(false);
            $table->dateTime('is_problem_at')->nullable();

            $table->string('external_id')->nullable();
            $table->text('error_external_id')->nullable();
            $table->integer('tariff_id')->nullable();
            $table->integer('point_id')->nullable();
            $table->string('number');

            $table->json('delivery_address')->nullable();
            $table->string('receiver_name')->nullable();
            $table->string('receiver_phone')->nullable();
            $table->string('receiver_email')->nullable();

            $table->decimal('cost', 18, 4)->default(0.0);
            $table->decimal('width', 18, 4)->default(0.0);
            $table->decimal('height', 18, 4)->default(0.0);
            $table->decimal('length', 18, 4)->default(0.0);
            $table->decimal('weight', 18, 4)->default(0.0);
            $table->dateTime('delivery_at')->nullable();
            $table->dateTime('status_at')->nullable();
            $table->dateTime('status_external_id_at')->nullable();

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
        });

        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('delivery_id');
            $table->unsignedBigInteger('merchant_id');
            $table->unsignedTinyInteger('delivery_service_zero_mile')->nullable();
            $table->unsignedBigInteger('store_id');
            $table->tinyInteger('status', false, true)->default(1);
            $table->dateTime('status_at')->nullable();
            $table->unsignedTinyInteger('payment_status')->default(1);
            $table->dateTime('payment_status_at')->nullable();
            $table->boolean('is_canceled')->default(false);
            $table->dateTime('is_canceled_at')->nullable();
            $table->boolean('is_problem')->default(false);
            $table->dateTime('is_problem_at')->nullable();

            $table->string('number');
            $table->decimal('cost', 18, 4)->default(0.0);
            $table->decimal('width', 18, 4)->default(0.0);
            $table->decimal('height', 18, 4)->default(0.0);
            $table->decimal('length', 18, 4)->default(0.0);
            $table->decimal('weight', 18, 4)->default(0.0);
            $table->timestamp('required_shipping_at');
            $table->text('assembly_problem_comment')->nullable();

            $table->timestamps();

            $table->foreign('delivery_id')->references('id')->on('delivery');
        });

        Schema::create('shipment_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shipment_id');
            $table->unsignedBigInteger('basket_item_id');

            $table->timestamps();

            $table->foreign('shipment_id')->references('id')->on('shipments');
            $table->foreign('basket_item_id')->references('id')->on('basket_items');

            $table->unique(['shipment_id', 'basket_item_id']);
        });

        Schema::create('orders_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->text('text')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
        });

        Schema::create('payment_options', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('merchant_id')->unsigned();
            $table->string('terminal_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_options');
        Schema::dropIfExists('shipment_items');
        Schema::dropIfExists('shipments');
        Schema::dropIfExists('delivery');
        Schema::dropIfExists('payments');
        Schema::dropIfExists('orders_comments');
        Schema::dropIfExists('history_main_entity');
        Schema::dropIfExists('history');
        Schema::dropIfExists('order_promo_codes');
        Schema::dropIfExists('order_discounts');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('basket_items');
        Schema::dropIfExists('baskets');
    }
};
