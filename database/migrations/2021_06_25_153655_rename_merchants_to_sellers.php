<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('baskets', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });

        Schema::table('payment_options', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('baskets', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });

        Schema::table('payment_options', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });
    }
};
