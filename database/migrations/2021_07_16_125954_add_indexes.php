<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('baskets', function (Blueprint $table) {
            $table->index(['updated_at']);
        });

        Schema::table('delivery', function (Blueprint $table) {
            $table->index(['receiver_name', 'receiver_phone', 'receiver_email']);
        });
        Schema::rename('delivery', 'deliveries');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('deliveries', 'delivery');
        Schema::table('delivery', function (Blueprint $table) {
            $table->dropIndex(['receiver_name', 'receiver_phone', 'receiver_email']);
        });
        Schema::table('baskets', function (Blueprint $table) {
            $table->dropIndex(['updated_at']);
        });
    }
};
