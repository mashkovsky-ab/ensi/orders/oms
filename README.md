# OMS

## Резюме

Название: OMS  
Домен: OMS  
Назначение: Сервис управления заказами  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

Регламент работы над задачами тоже находится в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/477528081)

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

| Название | Описание  | Переменные окружения |
| --- | --- | --- |
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| Kafka | Брокер сообщений. <br/>Producer осуществляет запись в следующие топики:<br/> - `<контур>.orders.fact.orders.1`<br/> - `<контур>.orders.fact.deliveries.1`<br/> Consumer слушает следующие топики:<br/> - `<контур>.logistic.fact.delivery-orders.1` | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| **Сервисы Ensi** | **Сервисы Ensi, с которыми данный сервис коммуницирует** |
| Logistic | Ensi Logistic | LOGISTICS_SERVICE_HOST |
| Catalog | Ensi Offers<br/>Ensi PIM | OFFERS_SERVICE_HOST<br/>PIM_SERVICE_HOST |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/oms/job/oms/  
URL: https://oms-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
