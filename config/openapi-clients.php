<?php

return [
    'catalog' => [
        'offers' => [
            'base_uri' => env('OFFERS_SERVICE_HOST') . "/api/v1",
        ],
        'pim' => [
            'base_uri' => env('PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'logistic' => [
        'logistic' => [
            'base_uri' => env('LOGISTICS_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
