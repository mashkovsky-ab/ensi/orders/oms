<?php

namespace Tests;

use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Api\StocksApi;
use Ensi\PimClient\Api\ProductsApi;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery\MockInterface;

class ComponentTestCase extends TestCase
{
    use DatabaseTransactions;

    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }

    protected function mockOffersStocksApi(): MockInterface|StocksApi
    {
        return $this->mock(StocksApi::class);
    }
}
