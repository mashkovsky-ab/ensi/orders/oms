RefundReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: идентификатор
      example: 1
    price:
      type: integer
      description: сумма возврата, коп.
      example: 10000
    is_partial:
      type: boolean
      description: является заявкой на частичный возврат
      example: true
    files:
      type: array
      items:
        $ref: '#/RefundFile'
    created_at:
      type: string
      format: date-time
      description: дата создания
      example: "2020-12-17T21:24:28.000000Z"
    updated_at:
      type: string
      format: date-time
      description: дата обновления
      example: "2021-04-23T11:04:57.000000Z"

RefundFile:
  type: object
  properties:
    id:
      type: integer
      description: идентификатор
      example: 1
    refund_id:
      type: integer
      description: идентификатор заявки на возврат
      example: 1
    original_name:
      type: string
      description: Оригинальное название файла
      example: "example.doc"
    file:
      $ref: '../../common_schemas.yaml#/File'
    created_at:
      type: string
      format: date-time
      description: дата загрузки файла
      example: "2020-12-17T21:24:28.000000Z"
    updated_at:
      type: string
      format: date-time
      description: дата обновления файла
      example: "2021-04-23T11:04:57.000000Z"

RefundOnlyCreateRequestProperties:
  type: object
  properties:
    order_items:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            description: идентификатор элемента корзины
            example: 1
          qty:
            type: number
            description: количество возвращаемых позиций
            example: 10
    refund_reason_ids:
      type: array
      items:
        type: integer
        description: идентификатор причины возврата
        example: 1

RefundCreateOnlyProperties:
  type: object
  properties:
    order_id:
      type: integer
      description: идентификатор заказа
      example: 1
    manager_id:
      type: integer
      description: идентификатор администратора (если автором был администратор)
      example: 1
      nullable: true
    source:
      type: integer
      description: источник взаимодействия (канал)
      example: 1
    user_comment:
      type: string
      description: комментарий пользователя
      example: "Прошу провести возврат средств"

RefundFillableProperties:
  type: object
  properties:
    status:
      type: integer
      description: статус заявки на возврат из RefundStatusEnum
      example: 1
    responsible_id:
      type: integer
      description: идентификатор ответственного
      example: 1
      nullable: true
    rejection_comment:
      type: string
      description: причина отклонения
      example: "Заявка на возврат отклонена по причине ..."
      nullable: true

RefundIncludes:
  type: object
  properties:
    order:
      $ref: '../../orders/schemas/orders.yaml#/Order'
    items:
      type: array
      items:
        $ref: '../../orders/schemas/order_items.yaml#/OrderItem'
    reasons:
      type: array
      items:
        $ref: './refund_reasons.yaml#/RefundReason'

Refund:
  allOf:
    - $ref: '#/RefundReadonlyProperties'
    - $ref: '#/RefundFillableProperties'
    - $ref: '#/RefundCreateOnlyProperties'
    - $ref: '#/RefundIncludes'

SearchRefundsRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    filter:
      type: object
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

SearchRefundsResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/Refund'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta

RefundFileResponse:
  type: object
  properties:
    data:
      $ref: '#/RefundFile'
    meta:
      type: object
  required:
    - data

RefundResponse:
  type: object
  properties:
    data:
      $ref: '#/Refund'
    meta:
      type: object
  required:
    - data

RefundForPatch:
  allOf:
    - $ref: '#/RefundFillableProperties'

RefundForDeleteFiles:
  type: object
  properties:
    file_ids:
      type: array
      items:
        type: integer
        description: Идентификатор вложения
        example: 1

RefundRequiredProperties:
  type: object
  required:
    - user_comment
    - source
    - order_id
    - order_items
    - refund_reason_ids

RefundForCreate:
  allOf:
    - $ref: '#/RefundCreateOnlyProperties'
    - $ref: '#/RefundFillableProperties'
    - $ref: '#/RefundOnlyCreateRequestProperties'
    - $ref: '#/RefundRequiredProperties'
