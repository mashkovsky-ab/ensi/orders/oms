<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

class OrderStatusEnum
{
    public const WAIT_PAY = 1;
    public const NEW = 10;
    public const APPROVED = 20;
    public const DONE = 100;
    public const CANCELED = 200;

    /**
     * @return int[]
     */
    public static function cases(): array
    {
        return [
            self::WAIT_PAY,
            self::NEW,
            self::APPROVED,
            self::DONE,
            self::CANCELED,
        ];
    }
}
