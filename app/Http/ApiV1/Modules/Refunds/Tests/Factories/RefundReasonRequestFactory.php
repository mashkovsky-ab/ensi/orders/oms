<?php

namespace App\Http\ApiV1\Modules\Refunds\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class RefundReasonRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'name' => $this->faker->text(255),
            'code' => $this->faker->text(20),
            'description' => $this->faker->optional()->text(1000),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
