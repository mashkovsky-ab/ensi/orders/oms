<?php

namespace App\Http\ApiV1\Modules\Refunds\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Illuminate\Database\Eloquent\Collection;

class RefundCreateRequestFactory extends BaseApiFactory
{
    public array $orderItems;
    public array $reasonIds;

    protected function definition(): array
    {
        return [
            'order_id' => $this->optionalId(),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'responsible_id' => $this->faker->optional()->randomNumber(),
            'source' => $this->faker->randomElement(OrderSourceEnum::cases()),
            'status' => $this->faker->randomElement(RefundStatusEnum::cases()),
            'user_comment' => $this->faker->text(100),
            'rejection_comment' => $this->faker->optional()->text(100),
            'order_items' => $this->orderItems,
            'refund_reason_ids' => $this->reasonIds,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withItems(Collection $items, bool $isFullRefund): self
    {
        $this->orderItems = $items
            ->map(
                fn ($item) => [
                    'id' => $item['id'],
                    'qty' => $isFullRefund ? $item['qty'] : $item['qty'] / 2,
                ]
            )
            ->values()
            ->toArray();

        return $this;
    }

    public function withReasons(Collection $reasons): self
    {
        $this->reasonIds = $reasons->pluck('id')->toArray();

        return $this;
    }
}
