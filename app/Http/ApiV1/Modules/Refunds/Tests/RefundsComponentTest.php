<?php

use App\Domain\Common\Models\Setting;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use App\Domain\Refunds\Models\Refund;
use App\Domain\Refunds\Models\RefundReason;
use App\Http\ApiV1\Modules\Refunds\Tests\Factories\RefundCreateRequestFactory;
use App\Http\ApiV1\Modules\Refunds\Tests\Factories\RefundPatchRequestFactory;
use App\Http\ApiV1\Modules\Refunds\Tests\Factories\RefundReasonRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Support\Carbon;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/refunds/refund-statuses success', function () {
    getJson("/api/v1/refunds/refund-statuses")
        ->assertStatus(200)
        ->assertJsonCount(count(RefundStatusEnum::cases()), 'data');
});

test('GET /api/v1/refunds/refund-reasons success', function () {
    $count = 5;
    RefundReason::factory()->count($count)->create();

    getJson("/api/v1/refunds/refund-reasons")
        ->assertStatus(200)
        ->assertJsonCount($count, 'data');
});

test('PATCH /api/v1/refunds/refund-reasons/{id} success', function () {
    /** @var RefundReason $reason */
    $reason = RefundReason::factory()->create([
        'code' => 'REASON_CODE',
        'name' => 'Reason name',
    ]);

    $name = 'New reason name';

    $reasonData = RefundReasonRequestFactory::new()->only(['name'])->make(['name' => $name]);

    patchJson("/api/v1/refunds/refund-reasons/{$reason->id}", $reasonData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $reason->id)
        ->assertJsonPath('data.name', $name)
        ->assertJsonPath('data.code', $reason->code);

    assertDatabaseHas((new RefundReason())->getTable(), [
        'id' => $reason->id,
        'name' => $name,
        'code' => $reason->code,
    ]);
});

test('POST /api/v1/refunds/refund-reasons success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $reason = RefundReasonRequestFactory::new()->make();

    postJson("/api/v1/refunds/refund-reasons", $reason)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'description']])
        ->assertJsonPath('data.name', $reason['name'])
        ->assertJsonPath('data.code', $reason['code']);

    assertDatabaseHas((new RefundReason())->getTable(), [
        'name' => $reason['name'],
        'code' => $reason['code'],
    ]);
});

test('POST /api/v1/refunds/refunds success', function (bool $fullRefund, int $status) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    $order = Order::factory()->create(['is_return' => false, 'is_partial_return' => false]);
    $delivery = Delivery::factory()->for($order)->done()->create();
    $shipment = Shipment::factory()->for($delivery)->create();
    $orderItems = OrderItem::factory()->count(2)->for($shipment)->for($order)->create();
    $reasons = RefundReason::factory()->count(2)->create();

    $refund = RefundCreateRequestFactory::new()
        ->withItems($orderItems, $fullRefund)
        ->withReasons($reasons)
        ->make([
            'order_id' => $order->id,
            'status' => $status,
        ]);

    postJson('/api/v1/refunds/refunds', $refund)
        ->assertStatus(201)
        ->assertJsonPath('data.order_id', $refund['order_id'])
        ->assertJsonPath('data.is_partial', !$fullRefund);

    assertDatabaseHas((new Refund())->getTable(), [
        'order_id' => $refund['order_id'],
        'is_partial' => !$fullRefund,
    ]);
})->with([
    'is full approved refund' => [true, RefundStatusEnum::APPROVED],
    'is partial approved refund' => [false, RefundStatusEnum::APPROVED],
    'is full new refund' => [true, RefundStatusEnum::NEW],
    'is partial new refund' => [false, RefundStatusEnum::NEW],
]);

test('POST /api/v1/refunds/refunds exceeded return time', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    $minutes = Setting::getValue(SettingCodeEnum::REFUND);
    $order = Order::factory()->create([
        'is_return' => false,
        'is_partial_return' => false,
        'created_at' => Carbon::now()->subMinutes($minutes)->subSecond()->format('Y-m-d H:i:s'),
    ]);
    $delivery = Delivery::factory()->for($order)->done()->create();
    $shipment = Shipment::factory()->for($delivery)->create();
    $orderItems = OrderItem::factory()->count(2)->for($shipment)->for($order)->create();
    $reasons = RefundReason::factory()->count(2)->create();

    $refund = RefundCreateRequestFactory::new()
        ->withItems($orderItems, true)
        ->withReasons($reasons)
        ->make([
            'order_id' => $order->id,
            'status' => RefundStatusEnum::APPROVED,
        ]);

    postJson('/api/v1/refunds/refunds', $refund)
        ->assertStatus(400);

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'is_return' => false,
        'is_partial_return' => false,
    ]);

    assertDatabaseMissing((new Refund())->getTable(), [
        'order_id' => $order->id,
    ]);
});

test('POST /api/v1/refunds/refunds:search success', function () {
    $refunds = Refund::factory()
        ->count(10)
        ->sequence(
            ['status' => RefundStatusEnum::APPROVED],
            ['status' => RefundStatusEnum::NEW],
        )
        ->create();

    postJson("/api/v1/refunds/refunds:search", [
        "filter" => ["status" => RefundStatusEnum::NEW],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $refunds->last()->id)
        ->assertJsonPath('data.0.status', RefundStatusEnum::NEW);
});


test('GET /api/v1/refunds/refunds/{id}?include=reasons success', function () {
    $count = 3;
    $reasons = RefundReason::factory()->count($count);

    /** @var Refund $refund */
    $refund = Refund::factory()
        ->hasAttached(
            factory: $reasons,
            relationship: 'reasons'
        )
        ->create();

    getJson("/api/v1/refunds/refunds/$refund->id?include=reasons")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['reasons' => [['id', 'code', 'name', 'description']]]])
        ->assertJsonCount($count, 'data.reasons');
});

test('GET /api/v1/refunds/refunds/{id}?include=items success', function () {
    $count = 3;
    $items = OrderItem::factory()->count($count);

    /** @var Refund $refund */
    $refund = Refund::factory()
        ->hasAttached(
            factory: $items,
            pivot: ['qty' => 1],
            relationship: 'items'
        )
        ->create();

    getJson("/api/v1/refunds/refunds/$refund->id?include=items")
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => ['items' => [['id', 'order_id', 'offer_id', 'name', 'qty', 'price', 'cost', 'refund_qty']]],
        ])
        ->assertJsonCount($count, 'data.items');
});

test('GET /api/v1/refunds/refunds/{id}?include=order success', function () {
    /** @var Order $order */
    $order = Order::factory()->create();

    /** @var Refund $refund */
    $refund = Refund::factory()->for($order)->create();

    getJson("/api/v1/refunds/refunds/$refund->id?include=order")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['order' => ['id', 'cost', 'price']]])
        ->assertJsonPath('data.order.id', $order->id)
        ->assertJsonPath('data.order.cost', $order->cost)
        ->assertJsonPath('data.order.price', $order->price);
});

test('PATCH /api/v1/refunds/refunds/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    /** @var Refund $refund */
    $refund = Refund::factory()->create([
        'status' => RefundStatusEnum::NEW,
        'responsible_id' => 123,
    ]);

    $status = RefundStatusEnum::APPROVED;

    $refundData = RefundPatchRequestFactory::new()->only(['status'])->make(['status' => $status]);

    patchJson("/api/v1/refunds/refunds/{$refund->id}", $refundData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $refund->id)
        ->assertJsonPath('data.status', $status)
        ->assertJsonPath('data.responsible_id', $refund->responsible_id);

    assertDatabaseHas((new Refund())->getTable(), [
        'id' => $refund->id,
        'status' => $status,
        'responsible_id' => $refund->responsible_id,
    ]);
});
