<?php

namespace App\Http\ApiV1\Modules\Refunds\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteRefundFilesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'file_ids'   => ['required', 'array', 'min:1'],
            'file_ids.*' => ['integer'],
        ];
    }

    public function getFileIds(): array
    {
        return $this->get('file_ids');
    }
}
