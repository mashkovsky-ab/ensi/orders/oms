<?php

namespace App\Http\ApiV1\Modules\Refunds\Requests;

use App\Domain\Orders\Models\Order;
use App\Domain\Refunds\Models\RefundReason;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateRefundRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_comment' => ['required', 'string'],
            'source' => ['required', Rule::in(OrderSourceEnum::cases())],
            'manager_id' => ['integer', 'nullable'],
            'order_id' => ['required', Rule::exists(Order::class, 'id')],
            'responsible_id' => ['nullable', 'integer'],
            'status' => [Rule::in(RefundStatusEnum::cases())],
            'rejection_comment' => ['nullable', 'string'],

            'order_items' => ['required', 'array'],
            'order_items.*.id' => ['required', 'integer'],
            'order_items.*.qty' => ['required', 'numeric', 'gt:0'],

            'refund_reason_ids' => ['required', 'array'],
            'refund_reason_ids.*' => ['required', Rule::exists(RefundReason::class, 'id')],
        ];
    }
}
