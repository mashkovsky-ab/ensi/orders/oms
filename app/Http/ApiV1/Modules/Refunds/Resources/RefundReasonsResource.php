<?php


namespace App\Http\ApiV1\Modules\Refunds\Resources;

use App\Domain\Refunds\Models\RefundReason;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class RefundReasonsResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin RefundReason
 */
class RefundReasonsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
