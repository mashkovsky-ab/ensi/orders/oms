<?php


namespace App\Http\ApiV1\Modules\Refunds\Resources;

use App\Domain\Refunds\Data\RefundStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class RefundStatusesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin RefundStatus
 */
class RefundStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
