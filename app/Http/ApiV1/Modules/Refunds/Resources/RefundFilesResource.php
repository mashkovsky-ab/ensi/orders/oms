<?php


namespace App\Http\ApiV1\Modules\Refunds\Resources;

use App\Domain\Refunds\Models\RefundFile;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class RefundFilesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin RefundFile
 */
class RefundFilesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'refund_id' => $this->refund_id,
            'original_name' => $this->original_name,
            'file' => $this->mapProtectedFileToResponse($this->path),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
