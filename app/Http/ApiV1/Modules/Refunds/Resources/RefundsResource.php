<?php


namespace App\Http\ApiV1\Modules\Refunds\Resources;

use App\Domain\Refunds\Models\Refund;
use App\Http\ApiV1\Modules\Orders\Resources\OrderItemsResource;
use App\Http\ApiV1\Modules\Orders\Resources\OrdersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class RefundsResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin Refund
 */
class RefundsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'manager_id' => $this->manager_id,
            'responsible_id' => $this->responsible_id,
            'source' => $this->source,
            'status' => $this->status,
            'price' => $this->price,
            'is_partial' => $this->is_partial,
            'user_comment' => $this->user_comment,
            'rejection_comment' => $this->rejection_comment,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'order' => new OrdersResource($this->whenLoaded('order')),
            'items' => OrderItemsResource::collection($this->whenLoaded('items')),
            'reasons' => RefundReasonsResource::collection($this->whenLoaded('reasons')),
            'files' => RefundFilesResource::collection($this->whenLoaded('files')),
        ];
    }
}
