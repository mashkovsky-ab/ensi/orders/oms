<?php


namespace App\Http\ApiV1\Modules\Refunds\Queries;

use App\Domain\Refunds\Models\Refund;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class RefundsQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Refund::query();

        parent::__construct($query);

        $this->allowedIncludes(['order', 'items', 'reasons', 'files']);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('order_id'),
            AllowedFilter::exact('manager_id'),
            AllowedFilter::exact('responsible_id'),
            AllowedFilter::exact('source'),
            AllowedFilter::exact('status'),
        ]);

        $this->defaultSort('id');
    }
}
