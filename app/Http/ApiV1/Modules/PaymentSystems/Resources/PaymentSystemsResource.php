<?php

namespace App\Http\ApiV1\Modules\PaymentSystems\Resources;

use App\Domain\PaymentSystems\Data\PaymentSystem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class PaymentSystemsResource
 * @package App\Http\ApiV1\Modules\PaymentSystems\Resources
 * @mixin PaymentSystem
 */
class PaymentSystemsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
