<?php

use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test("GET /api/v1/payment-systems/payment-systems success", function () {
    getJson("/api/v1/payment-systems/payment-systems")
        ->assertStatus(200)
        ->assertJsonCount(count(PaymentSystemEnum::cases()), 'data');
});
