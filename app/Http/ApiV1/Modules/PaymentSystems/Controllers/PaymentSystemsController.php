<?php

namespace App\Http\ApiV1\Modules\PaymentSystems\Controllers;

use App\Domain\PaymentSystems\Data\PaymentSystem;
use App\Http\ApiV1\Modules\PaymentSystems\Resources\PaymentSystemsResource;

class PaymentSystemsController
{
    public function getPaymentSystems()
    {
        return PaymentSystemsResource::collection(PaymentSystem::all());
    }
}
