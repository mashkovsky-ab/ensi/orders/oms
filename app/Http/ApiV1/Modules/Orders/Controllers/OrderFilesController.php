<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Order\AttachOrderFileAction;
use App\Domain\Orders\Actions\Order\DeleteOrderFilesAction;
use App\Http\ApiV1\Modules\Orders\Requests\AttachOrderFileRequest;
use App\Http\ApiV1\Modules\Orders\Requests\DeleteOrderFilesRequest;
use App\Http\ApiV1\Modules\Orders\Resources\OrderFilesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class OrderFilesController
{
    public function attach(int $id, AttachOrderFileRequest $request, AttachOrderFileAction $action)
    {
        return OrderFilesResource::make($action->execute($id, $request->getFile()));
    }

    public function delete(int $id, DeleteOrderFilesRequest $request, DeleteOrderFilesAction $action)
    {
        $action->execute($id, $request->getFileIds());

        return new EmptyResource();
    }
}
