<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\CommitOrder\CommitOrderAction;
use App\Domain\Orders\Actions\Order\AddOrderCommentAction;
use App\Domain\Orders\Actions\Order\ChangeOrderDeliveryAction;
use App\Domain\Orders\Actions\Order\ChangeOrderPaymentSystemAction;
use App\Domain\Orders\Actions\Order\CheckOrderPaymentStatusAction;
use App\Domain\Orders\Actions\Order\PatchOrderAction;
use App\Domain\Orders\Actions\Payment\PaymentSystem\StartPaymentAction;
use App\Domain\Orders\Data\OrderSource;
use App\Domain\Orders\Data\OrderStatus;
use App\Http\ApiV1\Modules\Orders\Queries\OrdersQuery;
use App\Http\ApiV1\Modules\Orders\Requests\AddOrderCommentRequest;
use App\Http\ApiV1\Modules\Orders\Requests\ChangeOrderDeliveryRequest;
use App\Http\ApiV1\Modules\Orders\Requests\ChangeOrderPaymentSystemRequest;
use App\Http\ApiV1\Modules\Orders\Requests\CommitOrderRequest;
use App\Http\ApiV1\Modules\Orders\Requests\PatchOrderRequest;
use App\Http\ApiV1\Modules\Orders\Requests\StartOrderPaymentRequest;
use App\Http\ApiV1\Modules\Orders\Resources\OrderSourcesResource;
use App\Http\ApiV1\Modules\Orders\Resources\OrdersResource;
use App\Http\ApiV1\Modules\Orders\Resources\OrderStatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\DataResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class OrdersController
{
    public function search(PageBuilderFactory $pageBuilderFactory, OrdersQuery $query)
    {
        return OrdersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, OrdersQuery $query)
    {
        return new OrdersResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchOrderRequest $request, PatchOrderAction $action)
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function changeDelivery(int $id, ChangeOrderDeliveryRequest $request, ChangeOrderDeliveryAction $action)
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function paymentCheck(int $id, CheckOrderPaymentStatusAction $action)
    {
        return new DataResource([
            'payment_status' => $action->execute($id),
        ]);
    }

    public function paymentStart(int $id, StartPaymentAction $action, StartOrderPaymentRequest $request, OrdersQuery $ordersQuery)
    {
        $link = $action->execute($ordersQuery->findOrFail($id), $request->getReturnUrl());

        return new DataResource([
            'payment_link' => $link,
        ]);
    }

    public function changePaymentSystem(int $id, ChangeOrderPaymentSystemAction $action, ChangeOrderPaymentSystemRequest $request, OrdersQuery $ordersQuery)
    {
        return new OrdersResource($action->execute($ordersQuery->findOrFail($id), $request->getPaymentSystem()));
    }

    public function addComment(int $id, AddOrderCommentRequest $request, AddOrderCommentAction $action)
    {
        $action->execute($id, $request->getText());

        return new EmptyResource();
    }

    public function commit(CommitOrderRequest $request, CommitOrderAction $action)
    {
        $order = $action->execute($request->convertToObject());

        return new DataResource([
            'order_id' => $order->id,
        ]);
    }

    public function getOrderStatuses()
    {
        return OrderStatusesResource::collection(OrderStatus::all());
    }

    public function getOrderSources()
    {
        return OrderSourcesResource::collection(OrderSource::all());
    }
}
