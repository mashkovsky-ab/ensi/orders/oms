<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Shipment\PatchShipmentAction;
use App\Domain\Orders\Data\ShipmentStatus;
use App\Http\ApiV1\Modules\Orders\Queries\ShipmentsQuery;
use App\Http\ApiV1\Modules\Orders\Requests\PatchShipmentRequest;
use App\Http\ApiV1\Modules\Orders\Resources\ShipmentsResource;
use App\Http\ApiV1\Modules\Orders\Resources\ShipmentStatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class ShipmentsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ShipmentsQuery $query)
    {
        return ShipmentsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, ShipmentsQuery $query)
    {
        return new ShipmentsResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchShipmentRequest $request, PatchShipmentAction $action)
    {
        return new ShipmentsResource($action->execute($id, $request->validated()));
    }

    public function getShipmentStatuses()
    {
        return ShipmentStatusesResource::collection(ShipmentStatus::all());
    }
}
