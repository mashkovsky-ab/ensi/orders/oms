<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Delivery\PatchDeliveryAction;
use App\Domain\Orders\Data\DeliveryStatus;
use App\Http\ApiV1\Modules\Orders\Queries\DeliveriesQuery;
use App\Http\ApiV1\Modules\Orders\Requests\PatchDeliveryRequest;
use App\Http\ApiV1\Modules\Orders\Resources\DeliveriesResource;
use App\Http\ApiV1\Modules\Orders\Resources\DeliveryStatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class DeliveriesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, DeliveriesQuery $query)
    {
        return DeliveriesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, DeliveriesQuery $query)
    {
        return new DeliveriesResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchDeliveryRequest $request, PatchDeliveryAction $action)
    {
        return new DeliveriesResource($action->execute($id, $request->validated()));
    }

    public function getDeliveryStatuses()
    {
        return DeliveryStatusesResource::collection(DeliveryStatus::all());
    }
}
