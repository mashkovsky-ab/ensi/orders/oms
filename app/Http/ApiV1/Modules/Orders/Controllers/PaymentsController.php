<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Payment\PaymentSystem\HandlePaymentAction;
use App\Domain\Orders\Data\PaymentMethod;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\PaymentSystems\Systems\Local\LocalPushPaymentData;
use App\Domain\PaymentSystems\Systems\Yandex\YandexPushPaymentData;
use App\Http\ApiV1\Modules\Orders\Resources\PaymentMethodsResource;
use App\Http\ApiV1\Modules\Orders\Resources\PaymentStatusesResource;
use Illuminate\Http\Request;

class PaymentsController
{
    public function handlerLocal(Request $request, HandlePaymentAction $action)
    {
        $action->execute(new LocalPushPaymentData($request->all()));

        return response('ok');
    }

    public function handlerYandex(Request $request, HandlePaymentAction $action)
    {
        $action->execute(new YandexPushPaymentData($request->all()));

        return response()->json([
            'processed' => 1,
        ]);
    }

    public function getPaymentMethods()
    {
        return PaymentMethodsResource::collection(PaymentMethod::all());
    }

    public function getPaymentStatuses()
    {
        return PaymentStatusesResource::collection(PaymentStatus::all());
    }
}
