<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DeliveriesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 *
 * @mixin Delivery
 */
class DeliveriesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'number' => $this->number,
            'status' => $this->status,
            'status_at' => $this->status_at,

            'cost' => $this->cost,
            'width' => $this->width,
            'height' => $this->height,
            'length' => $this->length,
            'weight' => $this->weight,

            'date' => $this->dateToIso($this->date),
            'timeslot' => $this->timeslot,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'shipments' => ShipmentsResource::collection($this->whenLoaded('shipments')),
        ];
    }
}
