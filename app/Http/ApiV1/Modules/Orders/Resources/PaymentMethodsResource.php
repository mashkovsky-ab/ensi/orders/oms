<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\PaymentMethod;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class PaymentMethodsResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin PaymentMethod
 */
class PaymentMethodsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
