<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\OrderStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class OrderStatusesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin OrderStatus
 */
class OrderStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
