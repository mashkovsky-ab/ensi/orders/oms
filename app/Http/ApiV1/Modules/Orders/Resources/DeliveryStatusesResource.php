<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\DeliveryStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DeliveryStatusesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin DeliveryStatus
 */
class DeliveryStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
