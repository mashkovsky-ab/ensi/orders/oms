<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Models\OrderItem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class OrderItemsResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin OrderItem
 */
class OrderItemsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'order_id' => $this->order_id,
            'offer_id' => $this->offer_id,
            'shipment_id' => $this->shipment_id,

            'name' => $this->name,
            'qty' => $this->qty,
            'price' => $this->price,
            'price_per_one' => $this->price_per_one,
            'cost' => $this->cost,
            'cost_per_one' => $this->cost_per_one,

            'refund_qty' => isset($this->refundItem) ? $this->refundItem->qty : new MissingValue(),

            'product_weight' => $this->product_weight,
            'product_weight_gross' => $this->product_weight_gross,
            'product_width' => $this->product_width,
            'product_height' => $this->product_height,
            'product_length' => $this->product_length,
            'product_barcode' => $this->product_barcode,
            'offer_external_id' => $this->offer_external_id,
            'offer_storage_address' => $this->offer_storage_address,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }
}
