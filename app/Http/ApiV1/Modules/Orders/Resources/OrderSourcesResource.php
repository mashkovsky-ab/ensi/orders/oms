<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\OrderSource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class OrderSourcesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin OrderSource
 */
class OrderSourcesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
