<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Models\OrderFile;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class OrderFilesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 *
 * @mixin OrderFile
 */
class OrderFilesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'order_id' => $this->order_id,
            'original_name' => $this->original_name,
            'file' => $this->mapProtectedFileToResponse($this->path),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
