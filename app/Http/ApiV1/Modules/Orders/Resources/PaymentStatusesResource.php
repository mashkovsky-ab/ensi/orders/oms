<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\PaymentStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class PaymentStatusesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin PaymentStatus
 */
class PaymentStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
