<?php


namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\ShipmentStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class ShipmentStatusesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin ShipmentStatus
 */
class ShipmentStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
