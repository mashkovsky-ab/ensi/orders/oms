<?php


namespace App\Http\ApiV1\Modules\Orders\Queries;

use App\Domain\Orders\Models\Order;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class OrdersQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Order::query();

        parent::__construct($query);

        $this->allowedIncludes([
            'items',
            'deliveries.shipments',
            AllowedInclude::relationship('deliveries.shipments.order_items', 'deliveries.shipments.orderItems'),
            'files',
        ]);

        $this->allowedSorts([
            'id',
            'number',
            'customer_email',
            'cost',
            'price',
            'spent_bonus',
            'added_bonus',
            'promo_code',
            'delivery_price',
            'delivery_cost',
            'receiver_name',
            'receiver_phone',
            'receiver_email',
            'status_at',
            'payment_status_at',
            'payed_at',
            'payment_expires_at',
            'is_expired',
            'is_expired_at',
            'is_return',
            'is_return_at',
            'is_partial_return',
            'is_partial_return_at',
            'is_problem',
            'is_problem_at',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('customer_id'),
            AllowedFilter::exact('number'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('payment_status'),
            AllowedFilter::exact('source'),
            AllowedFilter::exact('responsible_id'),
            AllowedFilter::exact('delivery_method'),
            AllowedFilter::exact('delivery_service'),
            AllowedFilter::exact('delivery_tariff_id'),
            AllowedFilter::exact('delivery_point_id'),
            AllowedFilter::exact('payment_method'),
            AllowedFilter::exact('payment_system'),
            AllowedFilter::exact('is_expired'),
            AllowedFilter::exact('is_return'),
            AllowedFilter::exact('is_partial_return'),
            AllowedFilter::exact('is_problem'),

            AllowedFilter::exact('deliveries.shipments.seller_id'),
            AllowedFilter::exact('deliveries.shipments.store_id'),

            AllowedFilter::scope('number_like'),
            AllowedFilter::scope('customer_email_like'),
            AllowedFilter::scope('cost_from'),
            AllowedFilter::scope('cost_to'),
            AllowedFilter::scope('price_from'),
            AllowedFilter::scope('price_to'),
            AllowedFilter::scope('spent_bonus_from'),
            AllowedFilter::scope('spent_bonus_to'),
            AllowedFilter::scope('added_bonus_from'),
            AllowedFilter::scope('added_bonus_to'),
            AllowedFilter::scope('promo_code_like'),
            AllowedFilter::scope('manager_comment_like'),
            AllowedFilter::scope('delivery_price_from'),
            AllowedFilter::scope('delivery_price_to'),
            AllowedFilter::scope('delivery_cost_from'),
            AllowedFilter::scope('delivery_cost_to'),
            AllowedFilter::scope('receiver_name_like'),
            AllowedFilter::scope('receiver_phone_like'),
            AllowedFilter::scope('receiver_email_like'),
            AllowedFilter::scope('status_at_from'),
            AllowedFilter::scope('status_at_to'),
            AllowedFilter::scope('payment_status_at_from'),
            AllowedFilter::scope('payment_status_at_to'),
            AllowedFilter::scope('payed_at_from'),
            AllowedFilter::scope('payed_at_to'),
            AllowedFilter::scope('payment_expires_at_from'),
            AllowedFilter::scope('payment_expires_at_to'),
            AllowedFilter::scope('payment_external_id_like'),
            AllowedFilter::scope('is_expired_at_from'),
            AllowedFilter::scope('is_expired_at_to'),
            AllowedFilter::scope('is_return_at_from'),
            AllowedFilter::scope('is_return_at_to'),
            AllowedFilter::scope('is_partial_return_at_from'),
            AllowedFilter::scope('is_partial_return_at_to'),
            AllowedFilter::scope('is_problem_at_from'),
            AllowedFilter::scope('is_problem_at_to'),
            AllowedFilter::scope('problem_comment_like'),
            AllowedFilter::scope('created_at_from'),
            AllowedFilter::scope('created_at_to'),
            AllowedFilter::scope('updated_at_from'),
            AllowedFilter::scope('updated_at_to'),
            AllowedFilter::scope('delivery_comment_like'),
            AllowedFilter::scope('client_comment_like'),
        ]);

        $this->defaultSort('id');
    }
}
