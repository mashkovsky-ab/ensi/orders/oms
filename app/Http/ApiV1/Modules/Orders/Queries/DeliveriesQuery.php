<?php


namespace App\Http\ApiV1\Modules\Orders\Queries;

use App\Domain\Orders\Models\Delivery;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveriesQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Delivery::query();

        parent::__construct($query);

        $this->allowedIncludes(['shipments']);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
        ]);

        $this->defaultSort('id');
    }
}
