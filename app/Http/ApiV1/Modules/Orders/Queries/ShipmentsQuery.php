<?php


namespace App\Http\ApiV1\Modules\Orders\Queries;

use App\Domain\Orders\Models\Shipment;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ShipmentsQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Shipment::query();

        parent::__construct($query);

        $this->allowedIncludes([
            'delivery',
            AllowedInclude::relationship('order_items', 'orderItems'),
        ]);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
        ]);

        $this->defaultSort('id');
    }
}
