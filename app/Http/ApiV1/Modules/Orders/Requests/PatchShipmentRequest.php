<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchShipmentRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'status' => [Rule::in(ShipmentStatusEnum::cases())],
        ];
    }
}
