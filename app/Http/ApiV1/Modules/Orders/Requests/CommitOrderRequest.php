<?php


namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Domain\Orders\Actions\CommitOrder\Data\CommitData;
use App\Domain\Orders\Actions\CommitOrder\Data\DeliveryData;
use App\Domain\Orders\Actions\CommitOrder\Data\OrderItemData;
use App\Domain\Orders\Actions\CommitOrder\Data\ShipmentData;
use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\Timeslot;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class CommitOrderRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return array_merge([
            'customer_id' => ['required', 'integer'],
            'customer_email' => ['required', 'string'],
            'payment_system_id' => ['required', Rule::in(PaymentSystemEnum::cases())],
            'source' => ['required', Rule::in(OrderSourceEnum::cases())],
            'spent_bonus' => ['nullable', 'integer'],
            'added_bonus' => ['nullable', 'integer'],
            'promo_code' => ['nullable', 'string'],
            'client_comment' => ['nullable', 'string'],

            'delivery_service' => ['required', 'integer'],
            'delivery_method' => ['required', 'integer'],
            'delivery_tariff_id' => ['required', 'integer'],
            'delivery_point_id' => ['nullable', 'integer'],
            'delivery_address' => ['nullable', 'array'],
            'delivery_price' => ['required', 'integer'],
            'delivery_cost' => ['required', 'integer'],
            'delivery_comment' => ['nullable', 'string'],

            'receiver_name' => ['required', 'string'],
            'receiver_phone' => ['required', new PhoneRule()],
            'receiver_email' => ['required', 'string'],

            'deliveries' => ['required', 'array'],
            'deliveries.*.date' => ['required', 'date_format:Y-m-d'],
            'deliveries.*.timeslot' => ['nullable', 'array'],
            'deliveries.*.cost' => ['required', 'integer'],
            'deliveries.*.shipments' => ['required', 'array'],
            'deliveries.*.shipments.*.seller_id' => ['required', 'integer'],
            'deliveries.*.shipments.*.store_id' => ['required', 'integer'],
            'deliveries.*.shipments.*.items' => ['required', 'array'],
            'deliveries.*.shipments.*.items.*.offer_id' => ['required', 'integer'],
            'deliveries.*.shipments.*.items.*.cost_per_one' => ['required', 'numeric'],
            'deliveries.*.shipments.*.items.*.price_per_one' => ['required', 'numeric'],
            'deliveries.*.shipments.*.items.*.qty' => ['required', 'numeric'],
        ]);
    }

    public function convertToObject(): CommitData
    {
        $order = new CommitData();
        $order->customerId = $this->get('customer_id');
        $order->customerEmail = $this->get('customer_email');
        $order->clientComment = $this->get('client_comment');
        $order->paymentSystemId = $this->get('payment_system_id');
        $order->source = $this->get('source');
        $order->spentBonus = $this->get('spent_bonus');
        $order->addedBonus = $this->get('added_bonus');
        $order->promoCode = $this->get('promo_code');

        $order->deliveryService = $this->get('delivery_service');
        $order->deliveryMethod = $this->get('delivery_method');
        $order->deliveryTariffId = $this->get('delivery_tariff_id');
        $order->deliveryPointId = $this->get('delivery_point_id');

        $deliveryAddress = $this->get('delivery_address');
        if ($deliveryAddress) {
            // Приходится валидировать отдельно от основной валидации из-за бага https://github.com/laravel/ideas/issues/1704
            $order->deliveryAddress = DeliveryAddress::validateOrFail($deliveryAddress);
        }
        $order->deliveryComment = $this->get('delivery_comment');
        $order->deliveryPrice = $this->get('delivery_price');
        $order->deliveryCost = $this->get('delivery_cost');

        $order->receiverName = $this->get('receiver_name');
        $order->receiverPhone = $this->get('receiver_phone');
        $order->receiverEmail = $this->get('receiver_email');

        $order->deliveries = collect();
        foreach ($this->get('deliveries', []) as $delivery) {
            $deliveryData = new DeliveryData();
            $deliveryData->date = Carbon::createFromFormat('Y-m-d', $delivery['date'])->startOfDay();
            $timeslot = $delivery['timeslot'] ?? null;
            if ($timeslot) {
                // Приходится валидировать отдельно от основной валидации из-за бага https://github.com/laravel/ideas/issues/1704
                $deliveryData->timeslot = Timeslot::validateOrFail($timeslot);
            }
            $deliveryData->cost = $delivery['cost'];

            foreach ($delivery['shipments'] as $shipment) {
                $shipmentData = new ShipmentData();
                $shipmentData->sellerId = $shipment['seller_id'];
                $shipmentData->storeId = $shipment['store_id'];

                foreach ($shipment['items'] as $items) {
                    $itemData = new OrderItemData();
                    $itemData->offerId = $items['offer_id'];
                    $itemData->costPerOne = $items['cost_per_one'];
                    $itemData->pricePerOne = $items['price_per_one'];
                    $itemData->qty = $items['qty'];

                    $shipmentData->items[] = $itemData;
                }

                $deliveryData->shipments[] = $shipmentData;
            }

            $order->deliveries->push($deliveryData);
        }

        return $order;
    }
}
