<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class ChangeOrderPaymentSystemRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'payment_system' => ['required', 'integer', Rule::in(PaymentSystemEnum::cases())],
        ];
    }

    public function getPaymentSystem(): string
    {
        return $this->get('payment_system');
    }
}
