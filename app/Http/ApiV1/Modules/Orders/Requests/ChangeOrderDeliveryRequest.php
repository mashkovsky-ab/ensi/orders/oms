<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Domain\Orders\Data\DeliveryAddress;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ChangeOrderDeliveryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service' => ['integer'],
            'delivery_method' => ['integer'],
            'delivery_tariff_id' => ['integer'],
            'delivery_point_id' => ['nullable', 'integer'],
            'delivery_address' => ['nullable', 'array'],
            'delivery_price' => ['integer'],
            'delivery_cost' => ['integer'],
            'delivery_comment' => ['nullable', 'string'],
        ];
    }

    public function validated($key = null, $default = null): array
    {
        $validated = parent::validated($key, $default);

        $deliveryAddress = $this->get('delivery_address');
        if ($deliveryAddress) {
            // Приходится валидировать отдельно от основной валидации из-за бага https://github.com/laravel/ideas/issues/1704
            $validated['delivery_address'] = DeliveryAddress::validateOrFail($deliveryAddress);
        }

        return $validated;
    }
}
