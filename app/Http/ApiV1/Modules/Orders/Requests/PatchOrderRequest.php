<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;
use Illuminate\Validation\Rule;

class PatchOrderRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'responsible_id' => ['nullable', 'integer'],
            'status' => [Rule::in(OrderStatusEnum::cases())],
            'client_comment' => ['nullable', 'string'],
            'receiver_name' => ['string'],
            'receiver_phone' => ['string', new PhoneRule()],
            'receiver_email' => ['string'],
            'is_problem' => ['boolean'],
            'problem_comment' => ['string'],
        ];
    }
}
