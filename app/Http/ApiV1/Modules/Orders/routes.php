<?php

use App\Http\ApiV1\Modules\Orders\Controllers\PaymentsController;
use Illuminate\Support\Facades\Route;

Route::post('payments:handler:local', [PaymentsController::class, 'handlerLocal'])->name('handler.localPayment');
Route::post('payments:handler:yandex', [PaymentsController::class, 'handlerYandex'])->name('handler.yandexPayment');
