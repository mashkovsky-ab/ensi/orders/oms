<?php

use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// region orders/payment-methods
test("GET /api/v1/orders/payment-methods success", function () {
    getJson("/api/v1/orders/payment-methods")
        ->assertStatus(200)
        ->assertJsonCount(count(PaymentMethodEnum::cases()), 'data');
});
// endregion

// region orders/payment-statuses
test("GET /api/v1/orders/payment-statuses success", function () {
    getJson("/api/v1/orders/payment-statuses")
        ->assertStatus(200)
        ->assertJsonCount(count(PaymentStatusEnum::cases()), 'data');
});
// endregion
