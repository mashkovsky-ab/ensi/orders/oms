<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderComment;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\OrderCommitRequestFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\OrderRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\OffersClient\Dto\Stock;
use Ensi\PimClient\Dto\IndexSearchProductsResponse;
use Ensi\PimClient\Dto\IndexSearchProductsResponseItems;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsResponse;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// region orders/orders:search
test("POST /api/v1/orders/orders:search success", function () {
    $orders = Order::factory()
        ->count(10)
        ->sequence(
            ['status' => OrderStatusEnum::DONE],
            ['status' => OrderStatusEnum::NEW],
        )
        ->create();

    postJson("/api/v1/orders/orders:search", [
        "filter" => ["status" => OrderStatusEnum::NEW],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $orders->last()->id)
        ->assertJsonPath('data.0.status', OrderStatusEnum::NEW);
});
test("POST /api/v1/orders/orders:search filter manager_comment_like success", function () {
    $comment = 'manager_comment_like';
    /** @var Order $order */
    $order = Order::factory()->create();
    OrderComment::factory()->for($order)->create(['text' => $comment]);
    Order::factory()->create();

    postJson("/api/v1/orders/orders:search", ["filter" => ["manager_comment_like" => $comment]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $order->id);
});
test("POST /api/v1/orders/orders:search filter success", function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Order $order */
    $order = Order::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/orders/orders:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $order->{$fieldKey}),
    ]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $order->id);
})->with([
    ['number', '100', 'number_like', '1'],
    ['source', 1, 'source', [1, 2]],
    ['customer_id', 1, 'customer_id', [1, 2]],
    ['responsible_id', 1, 'responsible_id', [1, 2]],
    ['customer_email', 'test@tmp.ru', 'customer_email_like', 'tmp'],
    ['cost', 5, 'cost_from', 4],
    ['cost', 5, 'cost_to', 6],
    ['price', 5, 'price_from', 4],
    ['price', 5, 'price_to', 6],
    ['spent_bonus', 5, 'spent_bonus_from', 4],
    ['spent_bonus', 5, 'spent_bonus_to', 6],
    ['added_bonus', 5, 'added_bonus_from', 4],
    ['added_bonus', 5, 'added_bonus_to', 6],
    ['promo_code', 'newyear2021', 'promo_code_like', 'ewy'],
    ['delivery_method', 1, 'delivery_method', [1, 2]],
    ['delivery_service', 1, 'delivery_service', [1, 2]],
    ['delivery_tariff_id', 1, 'delivery_tariff_id', [1, 2]],
    ['delivery_point_id', 1, 'delivery_point_id', [1, 2]],
    ['delivery_price', 5, 'delivery_price_from', 4],
    ['delivery_price', 5, 'delivery_price_to', 6],
    ['delivery_cost', 5, 'delivery_cost_from', 4],
    ['delivery_cost', 5, 'delivery_cost_to', 6],
    ['receiver_name', 'Имя Фамилия Отчество', 'receiver_name_like', 'отче'],
    ['receiver_phone', '+799988867', 'receiver_phone_like', '867'],
    ['receiver_email', 'test@tmp.ru', 'receiver_email_like', 'tmp'],
    ['status', 1, 'status', [1, 2]],
    ['status_at', now()->subDay(),  'status_at_from', now()->subDays(2)],
    ['status_at', now()->subDay(),  'status_at_to', now()->addDays(2)],
    ['payment_status', 1, 'payment_status', [1, 2]],
    ['payment_status_at', now()->subDay(),  'payment_status_at_from', now()->subDays(2)],
    ['payment_status_at', now()->subDay(),  'payment_status_at_to', now()->addDays(2)],
    ['payed_at', now()->subDay(),  'payed_at_from', now()->subDays(2)],
    ['payed_at', now()->subDay(),  'payed_at_to', now()->addDays(2)],
    ['payment_expires_at', now()->subDay(),  'payment_expires_at_from', now()->subDays(2)],
    ['payment_expires_at', now()->subDay(),  'payment_expires_at_to', now()->addDays(2)],
    ['payment_method', 1, 'payment_method', [1, 2]],
    ['payment_system', 1, 'payment_system', [1, 2]],
    ['payment_external_id', 'b0f-cs', 'payment_external_id_like', 'f-c'],
    ['is_expired'],
    ['is_expired_at', now()->subDay(),  'is_expired_at_from', now()->subDays(2)],
    ['is_expired_at', now()->subDay(),  'is_expired_at_to', now()->addDays(2)],
    ['is_return'],
    ['is_return_at', now()->subDay(),  'is_return_at_from', now()->subDays(2)],
    ['is_return_at', now()->subDay(),  'is_return_at_to', now()->addDays(2)],
    ['is_partial_return'],
    ['is_partial_return_at', now()->subDay(),  'is_partial_return_at_from', now()->subDays(2)],
    ['is_partial_return_at', now()->subDay(),  'is_partial_return_at_to', now()->addDays(2)],
    ['is_problem'],
    ['is_problem_at', now()->subDay(),  'is_problem_at_from', now()->subDays(2)],
    ['is_problem_at', now()->subDay(),  'is_problem_at_to', now()->addDays(2)],
    ['problem_comment', 'текст Проблемы', 'problem_comment_like', 'ст пр'],
    ['created_at', now()->subDay(),  'created_at_from', now()->subDays(2)],
    ['created_at', now()->subDay(),  'created_at_to', now()->addDays(2)],
    ['updated_at', now()->subDay(),  'updated_at_from', now()->subDays(2)],
    ['updated_at', now()->subDay(),  'updated_at_to', now()->addDays(2)],
    ['delivery_comment', 'текст Доставки', 'delivery_comment_like', 'дост'],
    ['client_comment', 'Клиент', 'client_comment_like', 'клие'],
]);
test("POST /api/v1/orders/orders:search sort success", function (string $sort) {
    Order::factory()->create();
    postJson("/api/v1/orders/orders:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'number',
    'customer_email',
    'cost',
    'price',
    'spent_bonus',
    'added_bonus',
    'promo_code',
    'delivery_price',
    'delivery_cost',
    'receiver_name',
    'receiver_phone',
    'receiver_email',
    'status_at',
    'payment_status_at',
    'payed_at',
    'payment_expires_at',
    'is_expired',
    'is_expired_at',
    'is_return',
    'is_return_at',
    'is_partial_return',
    'is_partial_return_at',
    'is_problem',
    'is_problem_at',
    'created_at',
    'updated_at',
]);
// endregion

// region orders/orders/{id}
test("GET /api/v1/orders/orders/{id} success", function () {
    /** @var Order $order */
    $order = Order::factory()->create();
    $deliveries = Delivery::factory()->for($order)->count(3)->create();

    getJson("/api/v1/orders/orders/{$order->id}/?include=deliveries")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $order->id)
        ->assertJsonCount($deliveries->count(), 'data.deliveries');
});

test('PATCH /api/v1/orders/orders/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryComment = "delivery comment";
    /** @var Order $order */
    $order = Order::factory()->create([
        'delivery_comment' => $deliveryComment,
        'client_comment' => "client comment",
        'status' => OrderStatusEnum::WAIT_PAY,
    ]);

    $orderData = OrderRequestFactory::new()->only(['client_comment'])->make(['client_comment' => 'test']);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    patchJson("/api/v1/orders/orders/{$order->id}", $orderData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $order->id)
        ->assertJsonPath('data.client_comment', $orderData['client_comment'])
        ->assertJsonPath('data.delivery_comment', $deliveryComment);

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'client_comment' => $orderData['client_comment'],
        'delivery_comment' => $deliveryComment,
    ]);
});
test('PATCH /api/v1/orders/orders/{id} client comment bad', function () {
    /** @var ApiV1ComponentTestCase $this */
    $comment = 'init';
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::APPROVED, 'client_comment' => $comment]);
    $orderData = OrderRequestFactory::new()->only(['client_comment'])->make(['client_comment' => 'test']);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    patchJson("/api/v1/orders/orders/{$order->id}", $orderData)->assertStatus(400);

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'client_comment' => $comment,
    ]);
});
// endregion

// region orders/orders/{id}:start-payment
test('POST /api/v1/orders/orders/{id}:start-payment success', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withoutPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:start-payment", ['return_url' => "https://my.site"])
        ->assertStatus(200);

    assertDatabaseMissing((new Order())->getTable(), [
        'id' => $order->id,
        'payment_link' => null,
    ]);
});
// endregion

// region orders/orders/{id}:change-delivery
test('POST /api/v1/orders/orders/{id}:change-delivery success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryPrice = 100000;
    $deliveryCost = 100000;

    /** @var Order $order */
    $order = Order::factory()->create([
        'delivery_price' => $deliveryPrice,
        'delivery_cost' => $deliveryCost,
        'payment_status' => PaymentStatusEnum::NOT_PAID,
        'status' => OrderStatusEnum::NEW,
    ]);

    $orderData = OrderRequestFactory::new()->only(['delivery_comment'])->make(['delivery_comment' => 'test']);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-delivery", $orderData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $order->id)
        ->assertJsonPath('data.delivery_comment', $orderData['delivery_comment'])
        ->assertJsonPath('data.delivery_price', $deliveryPrice)
        ->assertJsonPath('data.delivery_cost', $deliveryCost);

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'delivery_comment' => $orderData['delivery_comment'],
        'delivery_price' => $deliveryPrice,
        'delivery_cost' => $deliveryCost,
    ]);
});

test('POST /api/v1/orders/orders/{id}:change-delivery bad payment status', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create([
        'payment_status' => PaymentStatusEnum::PAID,
    ]);

    $orderData = OrderRequestFactory::new()->makeChangeDelivery();
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-delivery", $orderData)
        ->assertStatus(400);
});

test('POST /api/v1/orders/orders/{id}:change-delivery bad order status', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create([
        'status' => OrderStatusEnum::APPROVED,
    ]);

    $orderData = OrderRequestFactory::new()->makeChangeDelivery();
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-delivery", $orderData)
        ->assertStatus(400);
});
// endregion

// region orders/orders/{id}:change-payment-method
test('POST /api/v1/orders/orders/{id}:change-payment-method success', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withoutPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson("/api/v1/orders/orders/{$order->id}:change-payment-method", ['payment_system' => PaymentSystemEnum::CASH])
        ->assertStatus(200);

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'payment_system' => PaymentSystemEnum::CASH,
    ]);
});
test('POST /api/v1/orders/orders/{id}:change-payment-method bad', function () {
    /** @var Order $order */
    $order = Order::factory()->withPayed()->create();

    postJson("/api/v1/orders/orders/{$order->id}:change-payment-method", ['payment_system' => PaymentSystemEnum::TEST])
        ->assertStatus(400);

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'payment_system' => PaymentSystemEnum::CASH,
    ]);
});
// endregion

// region orders/orders/{id}:comment
test('PUT /api/v1/orders/orders/{id}:comment success', function () {
    /** @var Order $order */
    $order = Order::factory()->create();
    $text = 'text';

    putJson("/api/v1/orders/orders/{$order->id}:comment", ['text' => $text])
        ->assertStatus(200);

    assertDatabaseHas((new OrderComment())->getTable(), [
        'order_id' => $order->id,
        'text' => $text,
    ]);
});
// endregion

// region orders/orders:commit
test('POST /api/v1/orders/orders:commit success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderCommit = OrderCommitRequestFactory::new()->make();
    $items = collect($orderCommit['deliveries'])->pluck('shipments')->collapse()->pluck('items')->collapse();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');

    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([
            'data' => $items->map(function (array $item, $key) {
                return new Offer([
                    'id' => $item['offer_id'],
                    'product_id' => $key + 1,
                    'external_id' => "{$item['offer_id']}_external",
                    'storage_address' => "storage_address_{$item['offer_id']}",
                    'stocks' => [
                        new Stock(['qty' => $item['qty'] + 1]),
                    ],
                ]);
            }),
        ]),
    ]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => new SearchProductsResponse([
            'data' => $items->map(function (array $item, $key) {
                $productId = $key + 1;

                return new Product([
                    'id' => $productId,
                    'name' => "product {$productId}",
                    'weight' => 100,
                    'width' => 150,
                    'weight_gross' => 150,
                    'barcode' => '15048765454',
                    'height' => 150,
                    'length' => 150,
                    'allow_publish' => true,
                ]);
            }),
        ]),
    ]);
    $this->mockOffersStocksApi()->shouldReceive('reserveStocks');

    postJson("/api/v1/orders/orders:commit", $orderCommit)
        ->assertStatus(200);

    $itemsPrice = $items->sum(fn ($item) => ($item['price_per_one'] * $item['qty']));
    $itemsCost = $items->sum(fn ($item) => ($item['cost_per_one'] * $item['qty']));

    assertDatabaseHas((new Order())->getTable(), [
        'customer_id' => $orderCommit['customer_id'],
        'price' => round($itemsPrice) + $orderCommit['delivery_price'],
        'cost' => round($itemsCost) + $orderCommit['delivery_cost'],
    ]);
});
test('POST /api/v1/orders/orders:commit inactive product', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderCommit = OrderCommitRequestFactory::new()->make();
    $items = collect($orderCommit['deliveries'])->pluck('shipments')->collapse()->pluck('items')->collapse();

    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    $this->mockOffersOffersApi()->allows([
        'searchOffers' => new SearchOffersResponse([
            'data' => $items->map(function (array $item, $key) {
                return new Offer([
                    'id' => $item['offer_id'],
                    'product_id' => $key + 1,
                    'external_id' => "{$item['offer_id']}_external",
                    'storage_address' => "storage_address_{$item['offer_id']}",
                    'stocks' => [
                        new Stock(['qty' => $item['qty'] + 1]),
                    ],
                ]);
            }),
        ]),
    ]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => new SearchProductsResponse([
            'data' => $items->map(function (array $item, $key) {
                $productId = $key + 1;

                return new Product([
                    'id' => $productId,
                    'name' => "product {$productId}",
                    'weight' => 100,
                    'width' => 150,
                    'weight_gross' => 150,
                    'barcode' => '15048765454',
                    'height' => 150,
                    'length' => 150,
                    'allow_publish' => false,
                ]);
            }),
        ]),
    ]);
    $this->mockOffersStocksApi()->shouldReceive('reserveStocks');

    postJson("/api/v1/orders/orders:commit", $orderCommit)
        ->assertStatus(400);

    assertDatabaseMissing((new Order())->getTable(), [
        'customer_id' => $orderCommit['customer_id'],
    ]);
});
// endregion
// region orders/order-statuses
test("GET /api/v1/orders/order-statuses success", function () {
    getJson("/api/v1/orders/order-statuses")
        ->assertStatus(200)
        ->assertJsonCount(count(OrderStatusEnum::cases()), 'data');
});
// endregion
// region orders/order-sources
test("GET /api/v1/orders/order-sources success", function () {
    getJson("/api/v1/orders/order-sources")
        ->assertStatus(200)
        ->assertJsonCount(count(OrderSourceEnum::cases()), 'data');
});
// endregion
