<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\DeliveryRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// region orders/deliveries:search
test("POST /api/v1/orders/deliveries:search success", function () {
    $deliveries = Delivery::factory()->count(3)->create();
    $filterId = $deliveries->last()->id;

    postJson("/api/v1/orders/deliveries:search", [
        "filter" => ["id" => $filterId],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $filterId);
});
// endregion
// region orders/deliveries/{id}
test("GET /api/v1/orders/deliveries/{id} success", function () {
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create();
    $shipments = Shipment::factory()->for($delivery)->count(3)->create();

    getJson("/api/v1/orders/deliveries/{$delivery->id}?include=shipments")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $delivery->id)
        ->assertJsonCount($shipments->count(), 'data.shipments');
});

test("PATCH /api/v1/orders/deliveries/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create();
    $date = now()->startOfDay()->format(BaseJsonResource::DATE_FORMAT);

    $deliveryData = DeliveryRequestFactory::new()->only(['date'])->make(['date' => $date]);

    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');

    patchJson("/api/v1/orders/deliveries/{$delivery->id}", $deliveryData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $delivery->id)
        ->assertJsonPath('data.date', $date)
        ->assertJsonPath('data.status', $delivery->status);

    assertDatabaseHas((new Delivery())->getTable(), [
        'id' => $delivery->id,
        'date' => $date,
        'status' => $delivery->status,
    ]);
});
// endregion
// region orders/delivery-statuses
test("GET /api/v1/orders/delivery-statuses success", function () {
    getJson("/api/v1/orders/delivery-statuses")
        ->assertStatus(200)
        ->assertJsonCount(count(DeliveryStatusEnum::cases()), 'data');
});
// endregion
