<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class ShipmentRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'number' => $this->faker->numerify('######-#-#'),
            'status' => $this->faker->randomElement(ShipmentStatusEnum::cases()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
