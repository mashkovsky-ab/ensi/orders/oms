<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\Timeslot;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;

class OrderCommitRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $deliveryMethod = $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY;

        $deliveryPrice = $this->faker->numberBetween(0, 200);

        return [
            'source' => $this->faker->randomElement(OrderSourceEnum::cases()),

            'customer_id' => $this->faker->randomNumber(),
            'customer_email' => $this->faker->email(),
            'client_comment' => $this->faker->optional()->text(50),
            "payment_system_id" => $this->faker->randomElement(PaymentSystemEnum::cases()),
            'spent_bonus' => $this->faker->optional()->numberBetween(0, 10),
            'added_bonus' => $this->faker->optional()->numberBetween(0, 10),
            'promo_code' => $this->faker->optional()->word(),

            "delivery_service" => $this->faker->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            "delivery_method" => $deliveryMethod,
            "delivery_tariff_id" => $this->faker->randomNumber(),
            'delivery_point_id' => $isDelivery ? null : $this->faker->randomNumber(),
            'delivery_address' => $isDelivery ? DeliveryAddress::factory()->make()->toArray() : null,
            'delivery_comment' => $isDelivery ? $this->faker->optional()->text(50) : null,
            "delivery_price" => $deliveryPrice,
            "delivery_cost" => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),

            'receiver_name' => $this->faker->name(),
            'receiver_phone' => $this->faker->numerify('+7##########'),
            'receiver_email' => $this->faker->email(),

            "deliveries" => $this->definitionDeliveries(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    protected function definitionDeliveries(): array
    {
        $deliveries = [];
        for ($i = 0; $i < $this->faker->numberBetween(1, 3); $i++) {
            $shipments = [];
            for ($j = 0; $j < $this->faker->numberBetween(1, 3); $j++) {
                $items = [];
                for ($k = 0; $k < $this->faker->numberBetween(1, 3); $k++) {
                    $pricePerOne = $this->faker->numberBetween(1, 1000);
                    $items[] = [
                        'offer_id' => $this->faker->unique()->randomNumber(),
                        'price_per_one' => $pricePerOne,
                        'cost_per_one' => $this->faker->numberBetween($pricePerOne, $pricePerOne + 500),
                        'qty' => $this->faker->randomFloat(2, 1, 1000),
                    ];
                }
                $shipments[] = [
                    'seller_id' => $this->faker->randomNumber(),
                    'store_id' => $this->faker->randomNumber(),
                    'items' => $items,
                ];
            }
            $deliveries[] = [
                'date' => $this->faker->date(),
                'timeslot' => $this->faker->boolean() ? Timeslot::factory()->make()->toArray() : null,
                'cost' => $this->faker->numberBetween(1, 1000),
                'shipments' => $shipments,
            ];
        }

        return $deliveries;
    }
}
