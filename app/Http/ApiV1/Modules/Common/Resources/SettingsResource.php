<?php


namespace App\Http\ApiV1\Modules\Common\Resources;

use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class OrdersResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 *
 * @mixin Setting
 */
class SettingsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'value' => $this->value,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
