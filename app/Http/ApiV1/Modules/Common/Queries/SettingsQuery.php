<?php


namespace App\Http\ApiV1\Modules\Common\Queries;

use App\Domain\Common\Models\Setting;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SettingsQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Setting::query();

        parent::__construct($query);

        $this->allowedSorts(['id', 'code', 'value', 'created_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('value'),
        ]);

        $this->defaultSort('id');
    }
}
