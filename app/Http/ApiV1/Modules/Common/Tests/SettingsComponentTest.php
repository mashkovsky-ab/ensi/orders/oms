<?php

use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\Modules\Common\Tests\Factories\SettingsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/common/settings 200', function () {
    $setting = Setting::factory()->create();

    getJson("/api/v1/common/settings?filter[code]=" . $setting->code)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $setting->id);
});

test('GET /api/v1/common/settings 400', function () {
    $setting = Setting::factory()->create();

    getJson("/api/v1/common/settings?filter[name]=" . $setting->name)
        ->assertStatus(400);
});

test('PATCH /api/v1/common/settings 200', function () {
    $setting = Setting::factory()->create();

    $settingsQuery = SettingsRequestFactory::new()->make(["settings" => [['id' => $setting->id, 'value' => '123']]]);

    patchJson('/api/v1/common/settings', $settingsQuery)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $setting->id)
        ->assertJsonPath('data.0.value', $settingsQuery['settings'][0]['value']);

    assertDatabaseHas((new Setting())->getTable(), [
        'id' => $setting->id,
        'value' => $settingsQuery['settings'][0]['value'],
    ]);
});

test('PATCH /api/v1/common/settings 400', function () {
    $value = "20";
    $setting = Setting::factory()->create(['value' => $value]);

    $settingsQuery = SettingsRequestFactory::new()->make(["settings" => [['value' => "10"]]]);


    patchJson('/api/v1/common/settings', $settingsQuery)
        ->assertStatus(400);

    assertDatabaseHas((new Setting())->getTable(), [
        'id' => $setting->id,
        'value' => $value,
    ]);
});
