<?php

namespace App\Providers;

use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Events\ShipmentStatusUpdated;
use App\Domain\Orders\Listeners\CalcOrderIsRefund;
use App\Domain\Orders\Listeners\CancelChildrenFromOrder;
use App\Domain\Orders\Listeners\DonePaymentWhenOrderDone;
use App\Domain\Orders\Listeners\UpdateDeliveryStatusFromShipment;
use App\Domain\Orders\Listeners\UpdateOrderStatusFromDelivery;
use App\Domain\Refunds\Events\RefundStatusSaving;
use App\Domain\Refunds\Listeners\RefundPaymentWhenRefundApprove;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        ShipmentStatusUpdated::class => [
            UpdateDeliveryStatusFromShipment::class,
        ],
        DeliveryStatusUpdated::class => [
            UpdateOrderStatusFromDelivery::class,
        ],
        OrderStatusUpdated::class => [
            DonePaymentWhenOrderDone::class,
            CancelChildrenFromOrder::class,
        ],
        RefundStatusSaving::class => [
            CalcOrderIsRefund::class,
            RefundPaymentWhenRefundApprove::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
