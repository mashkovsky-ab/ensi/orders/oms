<?php

namespace App\Providers;

use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderFile;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use App\Domain\Orders\Observers\DeliveryObserver;
use App\Domain\Orders\Observers\OrderFileObserver;
use App\Domain\Orders\Observers\OrderItemObserver;
use App\Domain\Orders\Observers\OrderObserver;
use App\Domain\Orders\Observers\ShipmentObserver;
use App\Domain\Refunds\Models\RefundFile;
use App\Domain\Refunds\Observers\RefundFileObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use YooKassa\Client;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Model::preventLazyLoading(!app()->isProduction());
        $this->app->singleton(Client::class, function ($app) {
            $client = new Client();
            $client->setAuth(config('app.y_checkout_shop_id'), config('app.y_checkout_key'));

            return $client;
        });

        $this->addObservers();
    }

    protected function addObservers(): void
    {
        OrderItem::observe(OrderItemObserver::class);
        OrderFile::observe(OrderFileObserver::class);
        RefundFile::observe(RefundFileObserver::class);

        Order::observe(OrderObserver::class);

        Delivery::observe(DeliveryObserver::class);
        Shipment::observe(ShipmentObserver::class);
    }
}
