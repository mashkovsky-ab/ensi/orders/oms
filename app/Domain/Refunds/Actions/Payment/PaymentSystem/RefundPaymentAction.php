<?php


namespace App\Domain\Refunds\Actions\Payment\PaymentSystem;

use App\Domain\Refunds\Models\Refund;

class RefundPaymentAction
{
    public function execute(Refund $refund): void
    {
        // Тут пока непонятно как надо реализовывать, но пока мне кажется что в сущности Refund
        // будут поля под оплату, как у сущности Order и они будут заполняться какими-то данными.
        // Если система оплаты заказа будет поддерживать возвраты то будет автоматизация и интеграция,
        // а если нет, то выставление каких-нибудь ручных флагов
    }
}
