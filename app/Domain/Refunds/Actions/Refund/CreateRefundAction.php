<?php

namespace App\Domain\Refunds\Actions\Refund;

use App\Domain\Common\Models\Setting;
use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Refunds\Events\RefundStatusSaving;
use App\Domain\Refunds\Models\Refund;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class CreateRefundAction
{
    public function execute(array $fields): Refund
    {
        $this->checkPossibilityReturn($fields['order_id']);

        $refundQty = collect($fields['order_items'])->keyBy('id');
        $refundOrderItemIds = $refundQty->keys();

        # Все элементы корзины для заданного заказа
        $orderItems = OrderItem::query()
            ->with(['shipment', 'shipment.delivery'])
            ->where('order_id', $fields['order_id'])
            ->get()
            ->keyBy('id');

        # Элементы корзины на возврат
        $refundOrderItems = $orderItems->filter(
            fn (OrderItem $orderItem) => $refundOrderItemIds->search($orderItem->id) !== false
        );

        $this->checkOrderItemsRule($refundOrderItemIds, $orderItems);
        $this->checkDeliveredRule($refundOrderItems);
        $this->checkQtyRule($fields['order_id'], $refundOrderItems, $refundQty);

        # Сумма на возврат
        $price = $refundOrderItems->sum(fn (OrderItem $item) => ($item->price_per_one * $refundQty[$item->id]["qty"]));

        # Заявка на частичный возврат?
        $isPartials = $orderItems
            ->filter(fn (OrderItem $item) => ($item->qty > ($refundQty[$item->id]['qty'] ?? 0)))
            ->isNotEmpty();

        $refund = new Refund();
        $refund->order_id = $fields['order_id'];
        $refund->manager_id = $fields['manager_id'] ?? null;
        $refund->responsible_id = $fields['responsible_id'] ?? null;
        $refund->source = $fields['source'];
        $refund->status = $fields['status'] ?? RefundStatusEnum::NEW;
        $refund->price = round($price);
        $refund->is_partial = $isPartials;
        $refund->user_comment = $fields['user_comment'];
        $refund->rejection_comment = $fields['rejection_comment'] ?? null;
        $refund->save();

        $refundSyncItems = $refundQty->map(function ($item) {
            return ["qty" => $item["qty"]];
        });

        $refund->items()->sync($refundSyncItems);
        $refund->reasons()->sync($fields['refund_reason_ids']);

        RefundStatusSaving::dispatch($refund);

        return $refund;
    }

    public function checkPossibilityReturn(int $orderId)
    {
        /** @var Order $order */
        $order = Order::query()->findOrFail($orderId);
        $minutes = Setting::getValue(SettingCodeEnum::REFUND);

        if ($order->created_at < Carbon::now()->subMinutes($minutes)->format('Y-m-d H:i:s')) {
            throw new ValidateException("Время возврата превышено");
        }
    }

    /**
     * Количество возвращаемых позиций не может быть больше чем полученных товаров.
     * Учитываются товарные позиции, которые уже были возвращены, либо находятся на рассмотрении.
     */
    private function checkQtyRule(int $orderId, Collection $refundOrderItems, Collection $refundQty)
    {
        # Неотмененные заявки на возврат
        $refunds = Refund::query()->with('items')->active($orderId)->get();

        # Общее количество запрашиваемых товаров на возврат
        $request = $refundQty->pluck('qty', 'id');

        /** @var Refund $refund */
        foreach ($refunds as $refund) {
            foreach ($refund->items as $item) {
                if (!$request->has($item->id)) {
                    continue;
                }

                $request[$item->id] += $item->refundItem->qty;

                if ($request[$item->id] > $item->qty) {
                    throw new ValidateException("Количество возвращаемых позиций не может быть больше чем полученных товаров");
                }
            }
        }

        # Количество полученных товаров
        $limits = $refundOrderItems->pluck('qty', 'id');

        # Общее количество запрашиваемых товаров на возврат не должно превышать количество полученных товаров
        foreach ($refundQty as $id => $item) {
            if ($request[$id] > ($limits[$id] ?? 0)) {
                throw new ValidateException("Количество возвращаемых позиций не может быть больше чем полученных товаров");
            }
        }
    }

    /**
     * Все элементы корзины из заявки на возврат должны принадлежать $orderId
     * @param Collection $refundOrderItemIds
     * @param Collection $orderItems
     */
    private function checkOrderItemsRule(Collection $refundOrderItemIds, Collection $orderItems)
    {
        $refundOrderItemIds->each(function (int $itemId) use ($orderItems) {
            if (!$orderItems->has($itemId)) {
                throw new ValidateException("Все элементы корзины должны принадлежать заказу order_id");
            }
        });
    }

    /**
     * Все элементы корзины из заявки на возврат должны иметь статус доставки = Получен (DeliveryStatus::done())
     * @param Collection|OrderItem[] $refundOrderItems
     */
    private function checkDeliveredRule(Collection $refundOrderItems)
    {
        $refundOrderItems->each(function (OrderItem $item) {
            if (!in_array($item->shipment->delivery->status, DeliveryStatus::done())) {
                throw new ValidateException("Все элементы корзины из заявки на возврат должны иметь статус доставки = Получен");
            }
        });
    }
}
