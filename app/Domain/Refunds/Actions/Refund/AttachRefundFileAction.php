<?php

namespace App\Domain\Refunds\Actions\Refund;

use App\Domain\Refunds\Models\Refund;
use App\Domain\Refunds\Models\RefundFile;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RuntimeException;

class AttachRefundFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $id, UploadedFile $file): RefundFile
    {
        /** @var Refund $refund */
        $refund = Refund::query()->findOrFail($id);

        $hash = Str::random(20);
        $extension = $file->getClientOriginalExtension();
        $fileName = "{$refund->id}_{$hash}.{$extension}";
        $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);

        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk($this->fileManager->protectedDiskName());

        $path = $disk->putFileAs("refunds/{$hashedSubDirs}", $file, $fileName);
        if (!$path) {
            throw new RuntimeException("Unable to save file $fileName to directory refunds/{$hashedSubDirs}");
        }

        $refundFile            = new RefundFile();
        $refundFile->path      = $path;
        $refundFile->refund_id = $refund->id;
        $refundFile->original_name = $file->getClientOriginalName();
        $refundFile->save();

        return $refundFile;
    }
}
