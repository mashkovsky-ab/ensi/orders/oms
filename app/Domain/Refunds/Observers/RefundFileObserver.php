<?php

namespace App\Domain\Refunds\Observers;

use App\Domain\Refunds\Models\RefundFile;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Storage;

class RefundFileObserver
{
    public function __construct(protected EnsiFilesystemManager $filesystemManager)
    {
    }

    public function deleted(RefundFile $refundFile)
    {
        Storage::disk($this->filesystemManager->protectedDiskName())->delete($refundFile->path);
    }
}
