<?php

namespace App\Domain\Refunds\Models;

use App\Domain\Refunds\Models\Tests\Factories\RefundFileFactory;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property int    $id Идентификатор вложения для заявки на возврат
 * @property string $path Путь до файла
 * @property int    $refund_id Идентификатор заявки на возврат
 * @property string $original_name Оригинальное название файла
 * @property Carbon $created_at Дата создания
 * @property Carbon $updated_at Дата обновления
 */
class RefundFile extends Model implements Auditable
{
    use SupportsAudit;

    public static function factory(): RefundFileFactory
    {
        return RefundFileFactory::new();
    }

    public function refund(): BelongsTo
    {
        return $this->belongsTo(Refund::class);
    }
}
