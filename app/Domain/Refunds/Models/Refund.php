<?php

namespace App\Domain\Refunds\Models;

use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Refunds\Data\RefundStatus;
use App\Domain\Refunds\Models\Tests\Factories\RefundFactory;
use App\Exceptions\ValidateException;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Заявки на возврат
 *
 * @property int $id Идентификатор заявки
 * @property int $order_id Идентификатор заказа
 * @property int|null $manager_id Автор (если заявка создана менеджером)
 * @property int|null $responsible_id Ответственный
 * @property int $source Канал (источник взаимодействия)
 * @property int $status Статус заявки
 * @property int $price Сумма возврата (в коп.)
 * @property bool $is_partial Является частичным возвратом?
 * @property string $user_comment Комментарий пользователя
 * @property string|null $rejection_comment Причина отклонения
 * @property Carbon $created_at Дата создания
 * @property Carbon $updated_at Дата обновления
 *
 * @property-read Order $order Заказ
 * @property-read Collection|OrderItem[] $items Товары на возврат
 * @property-read Collection|RefundReason[] $reasons Причины возврата
 * @property-read Collection|RefundFile[] $files
 */
class Refund extends Model implements Auditable
{
    use SupportsAudit;

    protected $fillable = [
        'responsible_id',
        'status',
        'rejection_comment',
    ];

    public static function factory(): RefundFactory
    {
        return RefundFactory::new();
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function items(): BelongsToMany
    {
        return $this
            ->belongsToMany(OrderItem::class, RefundOrderItem::class)
            ->as('refundItem')
            ->withPivot(['qty'])
            ->using(RefundOrderItem::class);
    }

    public function reasons(): BelongsToMany
    {
        return $this->belongsToMany(RefundReason::class);
    }

    public function files(): HasMany
    {
        return $this->hasMany(RefundFile::class);
    }

    /**
     * Неотмененные заявки на возврат
     *
     * @param Builder $query
     * @param int     $orderId
     *
     * @return Builder
     */
    public function scopeActive(Builder $query, int $orderId): Builder
    {
        return $query
            ->where('order_id', $orderId)
            ->whereNotIn('status', RefundStatus::cancelled());
    }

    protected function setStatusAttribute(int $status)
    {
        if ($this->status && $this->status != $status && !in_array($status, RefundStatus::getAllowedNext($this))) {
            $newStatus = new RefundStatus($status);
            $oldStatus = new RefundStatus($this->status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['status'] = $status;
    }
}
