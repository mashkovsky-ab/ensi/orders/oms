<?php

namespace App\Domain\Refunds\Models;

use App\Domain\Refunds\Models\Tests\Factories\RefundReasonFactory;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * Причины возврата
 *
 * @property int $id Идентификатор причины возврата
 * @property string $code Символьный код причины возврата
 * @property string $name Наименование причины возврата
 * @property string $description Описание причины возврата
 * @property Carbon $created_at Дата создания
 * @property Carbon $updated_at Дата обновления
 */
class RefundReason extends Model implements Auditable
{
    use SupportsAudit;

    protected $fillable = [
        'code',
        'name',
        'description',
    ];

    public static function factory(): RefundReasonFactory
    {
        return RefundReasonFactory::new();
    }

    public function refunds(): BelongsToMany
    {
        return $this->belongsToMany(Refund::class);
    }
}
