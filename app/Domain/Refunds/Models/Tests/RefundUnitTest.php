<?php

use App\Domain\Refunds\Data\RefundStatus;
use App\Domain\Refunds\Models\Refund;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use function PHPUnit\Framework\assertEquals;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("Refund status update success", function () {
    /** @var Refund $refund */
    $refund = Refund::factory()->create(['status' => RefundStatusEnum::NEW]);
    $newStatus = RefundStatus::getAllowedNext($refund)[0];
    $refund->status = $newStatus;
    assertEquals($refund->status, $newStatus);
});

test("Refund status update bad", function () {
    /** @var Refund $refund */
    $refund = Refund::factory()->create(['status' => RefundStatusEnum::CANCELED]);
    $badStatuses = array_diff(RefundStatusEnum::cases(), RefundStatus::getAllowedNext($refund));
    $refund->status = current($badStatuses);
})->throws(ValidateException::class);
