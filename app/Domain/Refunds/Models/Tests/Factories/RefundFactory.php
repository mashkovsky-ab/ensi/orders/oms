<?php

namespace App\Domain\Refunds\Models\Tests\Factories;

use App\Domain\Orders\Models\Order;
use App\Domain\Refunds\Models\Refund;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefundFactory extends Factory
{
    protected $model = Refund::class;

    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'responsible_id' =>$this->faker->optional()->randomNumber(),
            'source' => $this->faker->randomElement(OrderSourceEnum::cases()),
            'status' => $this->faker->randomElement(RefundStatusEnum::cases()),
            'price' => $this->faker->numberBetween(1, 10000),
            'is_partial' => $this->faker->boolean(),
            'user_comment' => $this->faker->text(100),
            'rejection_comment' => $this->faker->optional()->text(100),
        ];
    }
}
