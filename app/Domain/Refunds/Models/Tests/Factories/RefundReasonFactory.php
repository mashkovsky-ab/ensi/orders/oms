<?php

namespace App\Domain\Refunds\Models\Tests\Factories;

use App\Domain\Refunds\Models\RefundReason;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefundReasonFactory extends Factory
{
    protected $model = RefundReason::class;

    public function definition()
    {
        return [
            'code' => $this->faker->text(20),
            'name' => $this->faker->text(255),
            'description' => $this->faker->optional()->text(200),
        ];
    }
}
