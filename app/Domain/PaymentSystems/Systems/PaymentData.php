<?php


namespace App\Domain\PaymentSystems\Systems;

use App\Domain\PaymentSystems\Systems\Local\LocalPaymentData;
use App\Domain\PaymentSystems\Systems\Yandex\YandexPaymentData;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Carbon\Carbon;

abstract class PaymentData
{
    public ?string $externalId;
    public ?string $paymentLink;
    public int $status;
    public ?Carbon $expiresAt;
    public ?Carbon $payedAt;

    // Свойства, которые заполняются не всегда
    /** @var ReceiptItemData[] */
    public array $receiptItems = [];
    public int $orderId;
    public string $customerEmail;

    public function __construct(array $data, protected $price)
    {
    }

    public static function getClassBySystemId(int $paymentSystemId): string
    {
        return match ($paymentSystemId) {
            PaymentSystemEnum::TEST => LocalPaymentData::class,
            PaymentSystemEnum::YANDEX => YandexPaymentData::class,
            default => throw new \Exception("Неизвестная система оплаты"),
        };
    }

    public function getData(): array
    {
        return [];
    }

    public function getPrice()
    {
        return $this->price;
    }
}
