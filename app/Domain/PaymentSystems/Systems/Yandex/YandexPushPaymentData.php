<?php


namespace App\Domain\PaymentSystems\Systems\Yandex;

use App\Domain\PaymentSystems\Systems\PushPaymentData;
use YooKassa\Client;
use YooKassa\Model\Notification\AbstractNotification;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;
use YooKassa\Model\NotificationEventType;

class YandexPushPaymentData extends PushPaymentData
{
    public $status;

    public function __construct(protected array $data)
    {
        parent::__construct($data);
        $this->loadPaymentInfo();
    }

    public function getNotification(): AbstractNotification
    {
        return ($this->data['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
            ? new NotificationSucceeded($this->data)
            : new NotificationWaitingForCapture($this->data);
    }

    protected function loadPaymentInfo()
    {
        $paymentInfo = resolve(Client::class)->getPaymentInfo(
            $this->getNotification()->getObject()->getId()
        );

        $this->externalId = $paymentInfo->id;
        $this->status = $paymentInfo->getStatus();
    }
}
