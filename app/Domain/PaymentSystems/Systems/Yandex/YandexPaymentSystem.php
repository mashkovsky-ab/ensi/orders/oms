<?php

namespace App\Domain\PaymentSystems\Systems\Yandex;

use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Domain\PaymentSystems\Systems\ReceiptItemData;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use YooKassa\Client;
use YooKassa\Model\PaymentStatus;

/**
 * Class YandexPaymentSystem
 * @package App\Domain\PaymentSystems\Systems
 */
class YandexPaymentSystem extends PaymentSystem
{
    public const CURRENCY_RUB = 'RUB';
    /** @var Client */
    private $yandexService;

    /**
     * YandexPaymentSystem constructor.
     */
    public function __construct()
    {
        $this->yandexService = resolve(Client::class);
    }

    protected static function amountFormat($price): array
    {
        return [
            'value' => number_format(
                num: $price / 100,
                decimals: 2,
                thousands_separator: '',
            ),
            'currency' => self::CURRENCY_RUB,
        ];
    }

    /**
     * @param YandexPaymentData $paymentData
     * @inheritDoc
     */
    public function createExternalPayment($paymentData, string $returnLink): void
    {
        $idempotenceKey = uniqid('', true);

        $response = $this->yandexService->createPayment([
            'amount' => [
                'value' => number_format($paymentData->getPrice(), 2, '.', ''),
                'currency' => self::CURRENCY_RUB,
            ],
            'payment_method_data' => [
                'type' => 'bank_card', // важно! при смене способа оплаты может поменяться максимальный срок холдирования
            ],
            'capture' => false,
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => $returnLink,
            ],
            'description' => "Заказ №{$paymentData->orderId}",
            'receipt' => [
                "tax_system_code" => "2",
                'email' => $paymentData->customerEmail,
                'items' => $this->generateItems($paymentData->receiptItems),
            ],
        ], $idempotenceKey);

        $paymentData->externalId = $response['id'];
        $paymentData->paymentLink = $response['confirmation']['confirmation_url'];
    }

    /**
     * @param YandexPaymentData $paymentData
     * @param YandexPushPaymentData $pushPaymentData
     * @inheritDoc
     */
    public function handlePushPayment($paymentData, $pushPaymentData): void
    {
        switch ($pushPaymentData->status) {
            case PaymentStatus::WAITING_FOR_CAPTURE:
                $paymentData->status = PaymentStatusEnum::HOLD;

                break;
            case PaymentStatus::SUCCEEDED:
                $paymentData->status = PaymentStatusEnum::PAID;
                $paymentData->payedAt = now();

                break;
            case PaymentStatus::CANCELED:
                $paymentData->status = PaymentStatusEnum::TIMEOUT;

                break;
        }
    }

    /**
     * @param YandexPaymentData $paymentData
     * @inheritDoc
     */
    public function commitHoldedPayment($paymentData)
    {
        $this->yandexService->capturePayment(
            [
                'amount' => [
                    'value' => $paymentData->getPrice(),
                    'currency' => self::CURRENCY_RUB,
                ],
                'receipt' => [
                    "tax_system_code" => "2",
                    'email' => $paymentData->customerEmail,
                    'items' => $this->generateItems($paymentData->receiptItems),
                ],
            ],
            $paymentData->externalId,
            uniqid('', true)
        );
    }

    /**
     * @param YandexPaymentData $paymentData
     * @inheritDoc
     */
    public function revert($paymentData)
    {
        $paymentData->status = PaymentStatusEnum::RETURNED;
    }

    /**
     * @param ReceiptItemData[] $receiptItems
     * @return array
     */
    protected function generateItems(array $receiptItems): array
    {
        $items = [];
        foreach ($receiptItems as $receiptItem) {
            $items[] = [
                'description' => $receiptItem->name,
                'quantity' => $receiptItem->qty,
                'amount' => self::amountFormat($receiptItem->price),
                'vat_code' => 1,
                "payment_mode" => "full_prepayment",
                "payment_subject" => "commodity",
            ];
        }

        return $items;
    }

    public function reinit($paymentData)
    {
        // TODO: Implement reinit() method.
    }
}
