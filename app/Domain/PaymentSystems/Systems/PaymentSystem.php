<?php

namespace App\Domain\PaymentSystems\Systems;

use App\Domain\PaymentSystems\Systems\Local\LocalPaymentSystem;
use App\Domain\PaymentSystems\Systems\Yandex\YandexPaymentSystem;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Exception;

/**
 * Interface PaymentSystem
 * @package App\Domain\PaymentSystems\Systems
 */
abstract class PaymentSystem
{
    public static function getById(int $paymentSystemId): PaymentSystem
    {
        return match ($paymentSystemId) {
            PaymentSystemEnum::TEST => new LocalPaymentSystem(),
            PaymentSystemEnum::YANDEX => new YandexPaymentSystem(),
            default => throw new Exception("Неизвестная система оплаты"),
        };
    }

    public static function getMethodById(int $paymentSystemId): int
    {
        return match ($paymentSystemId) {
            PaymentSystemEnum::TEST, PaymentSystemEnum::YANDEX => PaymentMethodEnum::ONLINE,
            PaymentSystemEnum::CASH => PaymentMethodEnum::OFFLINE,
            default => throw new Exception("Неизвестная система оплаты"),
        };
    }

    /**
     * Обратиться к внешней системе оплаты для создания платежа
     * @param PaymentData $paymentData
     * @param string $returnLink ссылка на страницу, на которую пользователь должен попасть после оплаты
     */
    abstract public function createExternalPayment($paymentData, string $returnLink): void;

    /**
     * Провести оплату холдированными средствами.
     * @param PaymentData $paymentData
     */
    abstract public function commitHoldedPayment($paymentData);

    /**
     * Пересоздание оплаты, после обновление стоимости заказа
     * @param $paymentData
     * @return mixed
     */
    abstract public function reinit($paymentData);

    /**
     * Отменить оплату.
     * @param PaymentData $paymentData
     */
    abstract public function revert($paymentData);

    /**
     * Получить данные от платёжной системы
     * @param PaymentData $paymentData
     */
    public function checkPayment($paymentData): void
    {
        throw new Exception("Система оплаты не поддерживает rest");
    }

    /**
     * Обработать данные от платёжной системы о совершении платежа
     * @param PaymentData $paymentData
     * @param PushPaymentData $pushPaymentData
     */
    public function handlePushPayment($paymentData, $pushPaymentData): void
    {
        throw new Exception("Система оплаты не поддерживает push");
    }
}
