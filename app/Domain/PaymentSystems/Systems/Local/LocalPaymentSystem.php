<?php

namespace App\Domain\PaymentSystems\Systems\Local;

use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * Class LocalPaymentSystem
 * @package App\Domain\PaymentSystems\Systems
 */
class LocalPaymentSystem extends PaymentSystem
{
    /**
     * @param LocalPaymentData $paymentData
     * @inheritDoc
     */
    public function createExternalPayment($paymentData, string $returnLink): void
    {
        $uuid = Uuid::uuid1()->toString();
        $paymentData->externalId = $uuid;
        $paymentData->paymentLink = route('paymentPage', ['paymentId' => $uuid]);
        $paymentData->returnLink = $returnLink;
        $paymentData->handlerUrl = route('handler.localPayment');
    }

    /**
     * @param LocalPaymentData $paymentData
     * @param LocalPushPaymentData $pushPaymentData
     * @inheritDoc
     */
    public function handlePushPayment($paymentData, $pushPaymentData): void
    {
        if ($pushPaymentData->status == LocalPushPaymentData::STATUS_DONE) {
            $paymentData->status = PaymentStatusEnum::PAID;
            $paymentData->payedAt = Carbon::now();
        }
    }

    /**
     * @param LocalPaymentData $paymentData
     * @inheritDoc
     */
    public function commitHoldedPayment($paymentData)
    {
        // TODO: Implement commitHoldedPayment() method.
    }

    /**
     * @param LocalPaymentData $paymentData
     * @inheritDoc
     */
    public function revert($paymentData)
    {
        $paymentData->status = PaymentStatusEnum::RETURNED;
    }

    public function reinit($paymentData)
    {
        // TODO: Implement reinit() method.
    }
}
