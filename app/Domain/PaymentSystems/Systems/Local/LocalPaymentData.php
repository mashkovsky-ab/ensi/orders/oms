<?php


namespace App\Domain\PaymentSystems\Systems\Local;

use App\Domain\PaymentSystems\Systems\PaymentData;

class LocalPaymentData extends PaymentData
{
    public ?string $returnLink;
    public ?string $handlerUrl;

    public function __construct(array $data, protected $price)
    {
        parent::__construct($data, $price);
        $this->returnLink = $data['returnLink'] ?? null;
        $this->handlerUrl = $data['handlerUrl'] ?? null;
    }

    public function getData(): array
    {
        return [
            'returnLink' => $this->returnLink,
            'handlerUrl' => $this->handlerUrl,
        ];
    }
}
