<?php


namespace App\Domain\PaymentSystems\Systems;

class PushPaymentData
{
    public string $externalId;

    public function __construct(protected array $data)
    {
    }
}
