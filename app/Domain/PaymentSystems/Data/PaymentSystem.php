<?php


namespace App\Domain\PaymentSystems\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;

class PaymentSystem
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            PaymentSystemEnum::YANDEX => 'Яндекс',
            PaymentSystemEnum::TEST => 'Тест',
            PaymentSystemEnum::CASH => 'Наличные',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];
        foreach (PaymentSystemEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
