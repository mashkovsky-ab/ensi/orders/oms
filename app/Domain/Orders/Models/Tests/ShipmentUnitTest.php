<?php

use App\Domain\Orders\Data\ShipmentStatus;
use App\Domain\Orders\Models\Shipment;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use function PHPUnit\Framework\assertEquals;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("Delivery status update success", function () {
    /** @var Shipment $shipment */
    $shipment = Shipment::factory()->create(['status' => ShipmentStatusEnum::NEW]);
    $newStatus = ShipmentStatus::getAllowedNext($shipment)[0];
    $shipment->status = $newStatus;
    assertEquals($shipment->status, $newStatus);
});

test("Delivery status update bad", function () {
    /** @var Shipment $shipment */
    $shipment = Shipment::factory()->create(['status' => ShipmentStatusEnum::NEW]);
    $badStatuses = array_diff(OrderStatusEnum::cases(), ShipmentStatus::getAllowedNext($shipment));
    $shipment->status = end($badStatuses);
})->throws(ValidateException::class);
