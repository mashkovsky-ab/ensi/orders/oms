<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShipmentFactory extends Factory
{
    protected $model = Shipment::class;

    public function definition()
    {
        return [
            'delivery_id' => Delivery::factory(),
            'seller_id' => $this->faker->randomNumber(),
            'store_id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(ShipmentStatusEnum::cases()),
            'number' => $this->faker->unique()->numerify('######-#-#'),
        ];
    }
}
