<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    protected $model = OrderItem::class;

    public function definition(): array
    {
        $pricePerOne = $this->faker->numberBetween(1, 10000);
        $costPerOne = $this->faker->numberBetween($pricePerOne, $pricePerOne + 1000);
        $qty = $this->faker->randomFloat(2, 1, 100);

        return [
            'offer_id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(50),
            'qty' => $qty,
            'price_per_one' => $pricePerOne,
            'cost_per_one' => $costPerOne,
            'product_weight' => $this->faker->numberBetween(1, 100),
            'product_weight_gross' => $this->faker->numberBetween(1, 100),
            'product_width' => $this->faker->numberBetween(1, 100),
            'product_height' => $this->faker->numberBetween(1, 100),
            'product_length' => $this->faker->numberBetween(1, 100),
            'product_barcode' => $this->faker->optional()->numerify('############'),
            'offer_external_id' => $this->faker->numerify('###-###'),
            'offer_storage_address' => $this->faker->optional()->numerify('###'),
            'order_id' => $this->faker->randomNumber(),
            'shipment_id' => Shipment::factory(),
        ];
    }
}
