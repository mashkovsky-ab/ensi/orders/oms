<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    protected $model = Order::class;

    public function definition()
    {
        $deliveryMethod = $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY;

        $status = $this->faker->randomElement(OrderStatusEnum::cases());

        $paymentSystem = $this->faker->randomElement(PaymentSystemEnum::cases());
        $paymentMethod = PaymentSystem::getMethodById($paymentSystem);

        $price = $this->faker->numberBetween(1000, 10000);
        $deliveryPrice = $this->faker->numberBetween(0, 200);

        if ($status == OrderStatusEnum::WAIT_PAY) {
            $paymentStatus = PaymentStatusEnum::NOT_PAID;
        } elseif (in_array($status, [OrderStatusEnum::NEW, OrderStatusEnum::APPROVED, OrderStatusEnum::DONE])) {
            $paymentStatus = $this->faker->randomElement([
                PaymentStatusEnum::HOLD,
                PaymentStatusEnum::PAID,
            ]);
        } else {
            $paymentStatus = $this->faker->randomElement([
                PaymentStatusEnum::RETURNED,
                PaymentStatusEnum::TIMEOUT,
                PaymentStatusEnum::CANCELED,
            ]);
        }

        $isPaymentStart = $paymentMethod == PaymentMethodEnum::ONLINE && (in_array($paymentStatus, [PaymentStatusEnum::PAID, PaymentStatusEnum::HOLD, PaymentStatusEnum::RETURNED,]) || $this->faker->boolean());

        return [
            'customer_id' => $this->faker->randomNumber(),
            'customer_email' => $this->faker->email(),
            'number' => $this->faker->unique()->numerify('######'),
            'price' => $price,
            'cost' => $this->faker->numberBetween($price, $price + 10000),
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
            'spent_bonus' => $this->faker->numberBetween(0, 10),
            'added_bonus' => $this->faker->numberBetween(0, 10),
            'promo_code' => $this->faker->optional()->word(),
            'delivery_comment' => $this->faker->optional()->text(50),
            'status' => $status,
            'is_problem' => $this->faker->boolean(),
            'problem_comment' => $this->faker->text(50),
            'is_expired' => $this->faker->boolean(),
            'is_return' => $this->faker->boolean(),
            'is_partial_return' => $this->faker->boolean(),
            'client_comment' => $this->faker->optional()->text(50),
            'delivery_method' => $deliveryMethod,
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'delivery_tariff_id' => $this->faker->randomNumber(),
            'delivery_point_id' => $isDelivery ? null : $this->faker->randomNumber(),
            'delivery_address' => $isDelivery ? DeliveryAddress::factory()->make() : null,
            'receiver_name' => $this->faker->name(),
            'receiver_phone' => $this->faker->numerify('+7##########'),
            'receiver_email' => $this->faker->email(),
            'payment_status' => $paymentStatus,
            'payed_at' => PaymentStatus::isPaid($paymentStatus) ? $this->faker->date() : null,
            'payment_system' => $paymentSystem,
            'payment_expires_at' => $isPaymentStart ? $this->faker->date() : null,
            'payment_link' => $isPaymentStart ? $this->faker->url() : null,
            'payment_external_id' => $isPaymentStart ? $this->faker->uuid() : null,
            'responsible_id' => $this->faker->optional()->randomNumber(),
            'source' => $this->faker->randomElement(OrderSourceEnum::cases()),
        ];
    }

    public function withoutPaymentStart(int $orderStatus = OrderStatusEnum::WAIT_PAY): self
    {
        return $this->state([
            'status' => $orderStatus,
            'payment_status' => PaymentStatusEnum::NOT_PAID,
            'payment_system' => PaymentSystemEnum::TEST,
            'payed_at' => null,
            'payment_expires_at' => null,
            'payment_link' => null,
            'payment_external_id' => null,
        ]);
    }

    public function withPaymentStart(int $orderStatus = OrderStatusEnum::WAIT_PAY): self
    {
        return $this->state([
            'status' => $orderStatus,
            'payment_status' => PaymentStatusEnum::NOT_PAID,
            'payment_system' => PaymentSystemEnum::TEST,
            'payed_at' => null,
            'payment_expires_at' => $this->faker->date(),
            'payment_link' => $this->faker->url(),
            'payment_external_id' => $this->faker->uuid(),
        ]);
    }

    public function withPayed(int $orderStatus = OrderStatusEnum::NEW): self
    {
        return $this->state([
            'status' => $orderStatus,
            'payment_status' => $this->faker->randomElement([
                PaymentStatusEnum::HOLD,
                PaymentStatusEnum::PAID,
            ]),
            'payment_system' => PaymentSystemEnum::CASH,
            'payed_at' => $this->faker->date(),
            'payment_expires_at' => null,
            'payment_link' => null,
            'payment_external_id' => null,
        ]);
    }

    public function withDeliveryAddress(): self
    {
        return $this->state([
            'delivery_address' => DeliveryAddress::factory()->make(),
        ]);
    }

    public function withPositiveDeliveryPrice(): self
    {
        $deliveryPrice = $this->faker->numberBetween(1, 200);

        return $this->state([
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
        ]);
    }
}
