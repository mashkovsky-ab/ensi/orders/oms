<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderComment;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderCommentFactory extends Factory
{
    protected $model = OrderComment::class;

    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'text' => $this->faker->text(50),
        ];
    }
}
