<?php

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Models\Delivery;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use function PHPUnit\Framework\assertEquals;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("Delivery status update success", function () {
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create(['status' => DeliveryStatusEnum::NEW]);
    $newStatus = DeliveryStatus::getAllowedNext($delivery)[0];
    $delivery->status = $newStatus;
    assertEquals($delivery->status, $newStatus);
});

test("Delivery status update bad", function () {
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create(['status' => DeliveryStatusEnum::NEW]);
    $badStatuses = array_diff(OrderStatusEnum::cases(), DeliveryStatus::getAllowedNext($delivery));
    $delivery->status = end($badStatuses);
})->throws(ValidateException::class);
