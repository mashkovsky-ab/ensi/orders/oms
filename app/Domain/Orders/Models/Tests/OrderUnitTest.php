<?php

use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use function PHPUnit\Framework\assertEquals;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("Order status update success", function () {
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::WAIT_PAY]);
    $newStatus = OrderStatus::getAllowedNext($order)[0];
    $order->status = $newStatus;
    assertEquals($order->status, $newStatus);
});

test("Order status update bad", function () {
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::WAIT_PAY]);
    $badStatuses = array_diff(OrderStatusEnum::cases(), OrderStatus::getAllowedNext($order));
    $order->status = end($badStatuses);
})->throws(ValidateException::class);

test("Order payment status update success", function () {
    /** @var Order $order */
    $order = Order::factory()->create(['payment_status' => PaymentStatusEnum::NOT_PAID]);
    $newStatus = PaymentStatus::getAllowedNext($order)[0];
    $order->payment_status = $newStatus;
    assertEquals($order->payment_status, $newStatus);
});

test("Order payment status update bad", function () {
    /** @var Order $order */
    $order = Order::factory()->create(['payment_status' => PaymentStatusEnum::NOT_PAID]);
    $badStatuses = array_diff(PaymentStatusEnum::cases(), PaymentStatus::getAllowedNext($order));
    $order->payment_status = end($badStatuses);
})->throws(ValidateException::class);
