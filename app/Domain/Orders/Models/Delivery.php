<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Data\Timeslot;
use App\Domain\Orders\Models\Tests\Factories\DeliveryFactory;
use App\Exceptions\ValidateException;
use Carbon\Carbon;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class Delivery
 * @package App\Models\Delivery
 *
 * @property int $id id отправления
 * @property int $order_id id заказа
 * @property string $number - номер отправления
 * @property int $cost - стоимость отправления, полученная от службы доставки (не влияет на общую стоимость доставки по заказу!) (коп.)
 *
 * @property int $status статус
 * @property Carbon $status_at
 *
 * @property Carbon $date
 * @property Timeslot|null $timeslot
 *
 * @property float $width - ширина (рассчитывается автоматически)
 * @property float $height - высота (рассчитывается автоматически)
 * @property float $length - длина (рассчитывается автоматически)
 * @property float $weight - вес (рассчитывается автоматически)
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Order $order - заказ
 * @property-read Collection|Shipment[] $shipments - отгрузки
 */
class Delivery extends Model implements Auditable
{
    use SupportsAudit;

    protected $fillable = [
        'date',
        'timeslot',
        'status',
    ];

    protected $table = 'deliveries';

    protected $casts = [
        'date' => 'date:Y-m-d',
        'status_at' => 'datetime',
        'weight' => 'float',
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
    ];

    /**
     * @param string $orderNumber - номер заказа
     * @param int $i - порядковый номер отправления в заказе
     * @return string
     */
    public static function makeNumber(string $orderNumber, int $i): string
    {
        return $orderNumber . '-' . $i;
    }

    public static function factory(): DeliveryFactory
    {
        return DeliveryFactory::new();
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function shipments(): HasMany
    {
        return $this->hasMany(Shipment::class);
    }

    protected function getTimeslotAttribute(): ?Timeslot
    {
        return $this->attributes['timeslot']
            ? new Timeslot(json_decode($this->attributes['timeslot']))
            : null;
    }

    protected function setTimeslotAttribute(?Timeslot $timeslot)
    {
        $this->attributes['timeslot'] = $timeslot?->toJson();
    }

    protected function setStatusAttribute(int $status)
    {
        if ($this->status && $this->status != $status && !in_array($status, DeliveryStatus::getAllowedNext($this))) {
            $newStatus = new DeliveryStatus($status);
            $oldStatus = new DeliveryStatus($this->status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['status'] = $status;
    }
}
