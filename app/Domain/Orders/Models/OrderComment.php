<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Models\Tests\Factories\OrderCommentFactory;
use Carbon\Carbon;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OrderComment
 *
 * @property int $id id комментария
 *
 * @property int $order_id id заказа
 * @property string $text текст комментария
 *
 * @property Carbon $created_at дата создания
 * @property Carbon $updated_at дата обновления
 *
 * @property-read Order $order
 */
class OrderComment extends Model implements Auditable
{
    use SupportsAudit;

    protected $table = 'orders_comments';

    const FILLABLE = [
        'text',
        'order_id',
    ];

    protected $fillable = self::FILLABLE;

    public static function factory(): OrderCommentFactory
    {
        return OrderCommentFactory::new();
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
