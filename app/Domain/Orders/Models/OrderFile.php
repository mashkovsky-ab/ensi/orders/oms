<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Models\Tests\Factories\OrderFileFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property int id
 * @property int $order_id
 * @property string $path
 * @property string $original_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Order $order
 */
class OrderFile extends Model
{
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public static function factory(): OrderFileFactory
    {
        return OrderFileFactory::new();
    }
}
