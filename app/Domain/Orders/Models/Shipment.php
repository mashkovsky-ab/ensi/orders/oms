<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Data\ShipmentStatus;
use App\Domain\Orders\Models\Tests\Factories\ShipmentFactory;
use App\Exceptions\ValidateException;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class Shipment
 * @package App\Models\Delivery
 * @property int $id идентификатор отгрузки
 * @property int $delivery_id ид отправления
 * @property int $seller_id ид продавца
 * @property int $store_id ид склада
 * @property string $number номер отгрузки
 * @property int $status статус отгрузки
 * @property Carbon|null $status_at дата установки статуса
 *
 * @property int $cost сумма товаров отгрузки (рассчитывается автоматически) (коп.)
 * @property float $width ширина (рассчитывается автоматически)
 * @property float $height высота (рассчитывается автоматически)
 * @property float $length длина (рассчитывается автоматически)
 * @property float $weight вес (рассчитывается автоматически)
 * @property Carbon $created_at дата создания
 * @property Carbon|null $updated_at дата обновления
 * @property-read Delivery $delivery отправление
 * @property-read Collection|OrderItem[] $orderItems
 */
class Shipment extends Model implements Auditable
{
    use SupportsAudit;

    const FILLABLE = [
        'status',
    ];

    protected $fillable = self::FILLABLE;

    /** @var string */
    protected $table = 'shipments';

    protected $dates = ['status_at'];

    protected $casts = [
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
        'weight' => 'float',
    ];

    /**
     * @param int $orderNumber - порядковый номер заказа
     * @param int $deliveryNumber - порядковый номер отправления
     * @param int $i - порядковый номер отгрузки в заказе
     * @return string
     */
    public static function makeNumber(int $orderNumber, int $deliveryNumber, int $i): string
    {
        return $orderNumber . '-' . $deliveryNumber . '-' . sprintf("%'02d", $i);
    }

    public static function factory(): ShipmentFactory
    {
        return ShipmentFactory::new();
    }

    public function delivery(): BelongsTo
    {
        return $this->belongsTo(Delivery::class);
    }

    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    protected function setStatusAttribute(int $status)
    {
        if ($this->status && $this->status != $status && !in_array($status, ShipmentStatus::getAllowedNext($this))) {
            $newStatus = new ShipmentStatus($status);
            $oldStatus = new ShipmentStatus($this->status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['status'] = $status;
    }
}
