<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Tests\Factories\OrderFactory;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Exceptions\ValidateException;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id id заказа
 * @property string $number номер заказа
 *
 * @property int|null $responsible_id id ответственного за заказ
 *
 * @property int $customer_id id покупателя
 * @property string $customer_email почта покупателя
 *
 * @property int $cost стоимость (без учета скидки) (рассчитывается автоматически) (коп.)
 * @property int $price стоимость (с учетом скидок) (рассчитывается автоматически) (коп.)
 *
 * @property int $delivery_service служба доставки
 * @property int $delivery_method метод доставки
 * @property int $delivery_cost стоимость доставки (без учета скидки) (коп.)
 * @property int $delivery_price стоимость доставки (с учетом скидки) (коп.)
 * @property int $delivery_tariff_id - идентификатор тарифа на доставку из сервиса логистики
 * @property int $delivery_point_id - идентификатор пункта самовывоза из сервиса логистики
 * @property DeliveryAddress $delivery_address - адрес доставки
 * @property string|null $delivery_comment комментарий к доставке
 *
 * @property string $receiver_name - имя получателя
 * @property string $receiver_phone - телефон получателя
 * @property string $receiver_email - e-mail получателя
 *
 * @property int $spent_bonus списано бонусов
 * @property int $added_bonus начислено бонусов
 * @property string|null $promo_code использованный промокод
 *
 * @property int $status статус
 * @property Carbon|null $status_at дата установки статуса заказа
 *
 * @property int $payment_status статус оплаты
 * @property Carbon|null $payment_status_at дата установки статуса оплаты
 * @property Carbon|null $payed_at дата оплаты
 * @property int $payment_system система оплаты
 * @property int $payment_method метод оплаты
 * @property Carbon|null $payment_expires_at дата, когда оплата станет просрочена
 * @property array $payment_data данные по оплате, зависят от выбранной системы оплаты
 * @property string|null $payment_link ссылка на оплату во внешней системе
 * @property string|null $payment_external_id id оплаты во внешней системе
 *
 * @property int $is_expired флаг, что заказ просроченный
 * @property Carbon|null $is_expired_at дата установки флага просроченного заказа
 * @property int $is_return флаг, что заказ возвращен
 * @property Carbon|null $is_return_at дата установки флага возвращенного заказа
 * @property int $is_partial_return флаг, что заказ частично возвращен
 * @property Carbon|null $is_partial_return_at дата установки флага частично возвращенного заказа
 * @property int $is_problem флаг, что заказ проблемный
 * @property Carbon|null $is_problem_at дата установки флага проблемного заказа
 * @property string $problem_comment последнее сообщение продавца о проблеме со сборкой
 *
 * @property int $source источник взаимодействия
 *
 * @property string|null $client_comment комментарий клиента
 *
 * @property Carbon|null $created_at дата создание
 * @property Carbon|null $updated_at дата обновления
 *
 * @property Collection|OrderItem[] $items корзина
 * @property Collection|Delivery[] $deliveries отправления заказа
 * @property Collection|OrderComment[] $comments Комментарии к заказу
 * @property Collection|OrderFile[] $files Вложения к заказу
 */
class Order extends Model implements Auditable
{
    use SupportsAudit;

    /** @var bool Флаг, который говорит нам что код находится в процессе создания заказа.
     * Нужен для того чтобы отключить некоторые действия в событиях.
     * Эти действия потом вручную реализуются в классе создания заказа
     * Пример: при создании ShipmentItem идёт пересчет веса/цены в Shipment. Это не нужно делать, пока не создадутся все ShipmentItems
     */
    public static $isCreating = false;

    protected $fillable = [
        'responsible_id',
        'status',
        'delivery_service',
        'delivery_method',
        'delivery_tariff_id',
        'delivery_point_id',
        'delivery_address',
        'delivery_price',
        'delivery_cost',
        'delivery_comment',
        'client_comment',
        'receiver_name',
        'receiver_phone',
        'receiver_email',
        'is_problem',
        'problem_comment',
    ];

    protected $casts = [
        'payment_data' => 'array',
    ];

    protected $dates = [
        'payment_status_at',
        'is_problem_at',
        'is_expired_at',
        'is_return_at',
        'is_partial_return_at',
        'status_at',
        'payed_at',
        'payment_expires_at',
    ];

    protected $hidden = ['payment_data'];

    public function __construct(array $attributes = [])
    {
        $this->payment_data = [];
        parent::__construct($attributes);
    }

    public static function factory(): OrderFactory
    {
        return OrderFactory::new();
    }

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function deliveries(): HasMany
    {
        return $this->hasMany(Delivery::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(OrderComment::class);
    }

    public function files(): HasMany
    {
        return $this->hasMany(OrderFile::class);
    }

    public function paymentSystem(): PaymentSystem
    {
        return PaymentSystem::getById($this->payment_system);
    }

    /**
     * Заказ оплачен?
     * @return bool
     */
    public function isPaid(): bool
    {
        return PaymentStatus::isPaid($this->payment_status);
    }

    public function scopeNumberLike(Builder $query, $number): Builder
    {
        return $query->where('number', 'ilike', "%{$number}%");
    }

    public function scopeCustomerEmailLike(Builder $query, $customerEmail): Builder
    {
        return $query->where('customer_email', 'ilike', "%{$customerEmail}%");
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }

    public function scopeUpdatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('updated_at', '>=', $date);
    }

    public function scopeUpdatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('updated_at', '<=', $date);
    }

    public function scopeCostFrom(Builder $query, $cost): Builder
    {
        return $query->where('cost', '>=', $cost);
    }

    public function scopeCostTo(Builder $query, $cost): Builder
    {
        return $query->where('cost', '<=', $cost);
    }

    public function scopePriceFrom(Builder $query, $price): Builder
    {
        return $query->where('price', '>=', $price);
    }

    public function scopePriceTo(Builder $query, $price): Builder
    {
        return $query->where('price', '<=', $price);
    }

    public function scopeSpentBonusFrom(Builder $query, $spentBonus): Builder
    {
        return $query->where('spent_bonus', '>=', $spentBonus);
    }

    public function scopeSpentBonusTo(Builder $query, $spentBonus): Builder
    {
        return $query->where('spent_bonus', '<=', $spentBonus);
    }

    public function scopeAddedBonusFrom(Builder $query, $addedBonus): Builder
    {
        return $query->where('added_bonus', '>=', $addedBonus);
    }

    public function scopeAddedBonusTo(Builder $query, $addedBonus): Builder
    {
        return $query->where('added_bonus', '<=', $addedBonus);
    }

    public function scopePromoCodeLike(Builder $query, $promoCode): Builder
    {
        return $query->where('promo_code', 'ilike', "%{$promoCode}%");
    }

    public function scopeDeliveryCostFrom(Builder $query, $deliveryCost): Builder
    {
        return $query->where('delivery_cost', '>=', $deliveryCost);
    }

    public function scopeDeliveryCostTo(Builder $query, $deliveryCost): Builder
    {
        return $query->where('delivery_cost', '<=', $deliveryCost);
    }

    public function scopeDeliveryPriceFrom(Builder $query, $deliveryPrice): Builder
    {
        return $query->where('delivery_price', '>=', $deliveryPrice);
    }

    public function scopeDeliveryPriceTo(Builder $query, $deliveryPrice): Builder
    {
        return $query->where('delivery_price', '<=', $deliveryPrice);
    }

    public function scopeReceiverNameLike(Builder $query, $receiverName): Builder
    {
        return $query->where('receiver_name', 'ilike', "%{$receiverName}%");
    }

    public function scopeReceiverPhoneLike(Builder $query, $receiverPhone): Builder
    {
        return $query->where('receiver_phone', 'ilike', "%{$receiverPhone}%");
    }

    public function scopeReceiverEmailLike(Builder $query, $receiverEmail): Builder
    {
        return $query->where('receiver_email', 'ilike', "%{$receiverEmail}%");
    }

    public function scopeStatusAtFrom(Builder $query, $date): Builder
    {
        return $query->where('status_at', '>=', $date);
    }

    public function scopeStatusAtTo(Builder $query, $date): Builder
    {
        return $query->where('status_at', '<=', $date);
    }

    public function scopePaymentStatusAtFrom(Builder $query, $date): Builder
    {
        return $query->where('payment_status_at', '>=', $date);
    }

    public function scopePaymentStatusAtTo(Builder $query, $date): Builder
    {
        return $query->where('payment_status_at', '<=', $date);
    }

    public function scopePayedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('payed_at', '>=', $date);
    }

    public function scopePayedAtTo(Builder $query, $date): Builder
    {
        return $query->where('payed_at', '<=', $date);
    }

    public function scopePaymentExpiresAtFrom(Builder $query, $date): Builder
    {
        return $query->where('payment_expires_at', '>=', $date);
    }

    public function scopePaymentExpiresAtTo(Builder $query, $date): Builder
    {
        return $query->where('payment_expires_at', '<=', $date);
    }

    public function scopePaymentExternalIdLike(Builder $query, $paymentExternalId): Builder
    {
        return $query->where('payment_external_id', 'ilike', "%{$paymentExternalId}%");
    }

    public function scopeIsExpiredAtFrom(Builder $query, $isExpiredAt): Builder
    {
        return $query->where('is_expired_at', '>=', $isExpiredAt);
    }

    public function scopeIsExpiredAtTo(Builder $query, $isExpiredAt): Builder
    {
        return $query->where('is_expired_at', '<=', $isExpiredAt);
    }

    public function scopeIsReturnAtFrom(Builder $query, $isReturnAt): Builder
    {
        return $query->where('is_return_at', '>=', $isReturnAt);
    }

    public function scopeIsReturnAtTo(Builder $query, $isReturnAt): Builder
    {
        return $query->where('is_return_at', '<=', $isReturnAt);
    }

    public function scopeIsPartialReturnAtFrom(Builder $query, $isPartialReturnAt): Builder
    {
        return $query->where('is_partial_return_at', '>=', $isPartialReturnAt);
    }

    public function scopeIsPartialReturnAtTo(Builder $query, $isPartialReturnAt): Builder
    {
        return $query->where('is_partial_return_at', '<=', $isPartialReturnAt);
    }

    public function scopeIsProblemAtFrom(Builder $query, $isProblemAt): Builder
    {
        return $query->where('is_problem_at', '>=', $isProblemAt);
    }

    public function scopeIsProblemAtTo(Builder $query, $isProblemAt): Builder
    {
        return $query->where('is_problem_at', '<=', $isProblemAt);
    }

    public function scopeProblemCommentLike(Builder $query, $problemComment): Builder
    {
        return $query->where('problem_comment', 'ilike', "%{$problemComment}%");
    }

    public function scopeDeliveryCommentLike(Builder $query, $deliveryComment): Builder
    {
        return $query->where('delivery_comment', 'ilike', "%{$deliveryComment}%");
    }

    public function scopeClientCommentLike(Builder $query, $clientComment): Builder
    {
        return $query->where('client_comment', 'ilike', "%{$clientComment}%");
    }

    public function scopeManagerCommentLike(Builder $query, $comment): Builder
    {
        return $query->whereHas('comments', function (Builder $query) use ($comment) {
            $query->where('text', 'ilike', "%{$comment}%");
        });
    }

    protected function getDeliveryAddressAttribute(): ?DeliveryAddress
    {
        return $this->attributes['delivery_address']
            ? new DeliveryAddress(json_decode($this->attributes['delivery_address']))
            : null;
    }

    protected function setDeliveryAddressAttribute(?DeliveryAddress $address)
    {
        $this->attributes['delivery_address'] = $address?->toJson();
    }

    protected function setStatusAttribute(int $status)
    {
        if ($this->status && $this->status != $status && !in_array($status, OrderStatus::getAllowedNext($this))) {
            $newStatus = new OrderStatus($status);
            $oldStatus = new OrderStatus($this->status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['status'] = $status;
    }

    protected function setPaymentStatusAttribute(int $paymentStatus)
    {
        if ($this->payment_status && $this->payment_status != $paymentStatus && !in_array($paymentStatus, PaymentStatus::getAllowedNext($this))) {
            $newStatus = new PaymentStatus($paymentStatus);
            $oldStatus = new PaymentStatus($this->payment_status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['payment_status'] = $paymentStatus;
    }
}
