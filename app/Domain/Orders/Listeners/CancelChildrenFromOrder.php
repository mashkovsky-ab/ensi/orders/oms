<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;

class CancelChildrenFromOrder
{
    public function handle(OrderStatusUpdated $event)
    {
        $order = $event->order;
        if (!in_array($order->status, OrderStatus::cancelled())) {
            return;
        }

        $order->loadMissing('deliveries.shipments');
        foreach ($order->deliveries as $delivery) {
            $delivery->status = DeliveryStatusEnum::CANCELED;
            $delivery->save();
            foreach ($delivery->shipments as $shipment) {
                $shipment->status = ShipmentStatusEnum::CANCELED;
                $shipment->save();
            }
        }
    }
}
