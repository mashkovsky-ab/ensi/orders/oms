<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Actions\Payment\PaymentSystem\ReinitPaymentAction;
use App\Domain\Orders\Events\OrderPricesUpdated;
use App\Domain\Orders\Models\Order;

/**
 * Подсчет стоимости заказа
 */
class CalcOrderPrice
{
    public function __construct(protected ReinitPaymentAction $reinitPaymentAction)
    {
    }

    public function handle(OrderPricesUpdated $event)
    {
        $order = $event->order;

        $price = 0;
        $cost = 0;

        $order->loadMissing('items');

        foreach ($order->items as $orderItem) {
            $price += $orderItem->price;
            $cost += $orderItem->cost;
        }

        $order->price = $price + $order->delivery_price;
        $order->cost = $cost + $order->delivery_cost;

        $order->save();

        if ($order->wasChanged(["price", "cost"])) {
            $this->reinitPaymentAction->execute($order);
        }
    }
}
