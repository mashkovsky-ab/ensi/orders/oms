<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Events\OrderPricesUpdated;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Events\ShipmentStatusUpdated;
use App\Domain\Orders\Listeners\CalcOrderPrice;
use App\Domain\Orders\Listeners\UpdateDeliveryStatusFromShipment;
use App\Domain\Orders\Listeners\UpdateOrderStatusFromDelivery;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Illuminate\Support\Facades\Event;
use function Pest\Laravel\assertDatabaseHas;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("UpdateDeliveryStatusFromShipment success", function (int $shipment1Status, int $shipment2NewStatus, int $deliveryCheckStatus) {
    /** @var IntegrationTestCase $this */
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create(['status' => DeliveryStatusEnum::NEW]);
    Shipment::factory()->for($delivery)->create(['status' => $shipment1Status]);
    /** @var Shipment $shipment2 */
    $shipment2 = Shipment::factory()->for($delivery)->create(['status' => $shipment2NewStatus]);

    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');
    Event::fake();

    resolve(UpdateDeliveryStatusFromShipment::class)->handle(new ShipmentStatusUpdated($shipment2));

    if ($deliveryCheckStatus != DeliveryStatusEnum::NEW) {
        Event::assertDispatched(DeliveryStatusUpdated::class);
    } else {
        Event::assertNotDispatched(DeliveryStatusUpdated::class);
    }

    assertDatabaseHas((new Delivery())->getTable(), [
        'id' => $delivery->id,
        'status' => $deliveryCheckStatus,
    ]);
})->with([
    'without changed' => [ShipmentStatusEnum::NEW, ShipmentStatusEnum::ASSEMBLED, DeliveryStatusEnum::NEW],
    'changed' => [ShipmentStatusEnum::ASSEMBLED, ShipmentStatusEnum::ASSEMBLED, DeliveryStatusEnum::ASSEMBLED],
]);

test("UpdateOrderStatusFromDelivery success", function (int $delivery1Status, int $delivery2NewStatus, int $orderCheckStatus) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::APPROVED]);
    Delivery::factory()->for($order)->create(['status' => $delivery1Status]);
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->for($order)->create(['status' => $delivery2NewStatus]);

    Event::fake();
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    resolve(UpdateOrderStatusFromDelivery::class)->handle(new DeliveryStatusUpdated($delivery));

    if ($orderCheckStatus != OrderStatusEnum::APPROVED) {
        Event::assertDispatched(OrderStatusUpdated::class);
    } else {
        Event::assertNotDispatched(OrderStatusUpdated::class);
    }

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'status' => $orderCheckStatus,
    ]);
})->with([
    'without changed' => [DeliveryStatusEnum::ASSEMBLED, DeliveryStatusEnum::DONE, OrderStatusEnum::APPROVED],
    'changed' => [DeliveryStatusEnum::DONE, DeliveryStatusEnum::DONE, OrderStatusEnum::DONE],
]);

test("CalcOrderPrice success", function () {
    /** @var IntegrationTestCase $this */

    /** @var Order $order */
    $order = Order::factory()->withoutPaymentStart()->withPositiveDeliveryPrice()->create(["cost" => 0, 'price' => 0]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    resolve(CalcOrderPrice::class)->handle(new OrderPricesUpdated($order));

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'price' => $order->delivery_price,
        'cost' => $order->delivery_cost,
    ]);
});
