<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;

class UpdateOrderStatusFromDelivery
{
    public function handle(DeliveryStatusUpdated $event)
    {
        $delivery = $event->delivery;
        if (!in_array($delivery->status, DeliveryStatus::done())) {
            return;
        }

        $delivery->loadMissing('order');
        if (in_array($delivery->order->status, OrderStatus::done())) {
            return;
        }

        $delivery->order->loadMissing('deliveries');
        foreach ($delivery->order->deliveries as $orderDelivery) {
            if ($orderDelivery->id == $delivery->id) { // Текущее отправление не смотрим вообще
                continue;
            }

            if (!in_array($orderDelivery->status, DeliveryStatus::done())) {
                return; // Если хоть у одного отправления статус - не собран, то ничего не делаем
            }
        }

        // Если мы тут, значит все отправления заказа имеют статус доставлено и заказу тоже нужно проставить соответствующий статус
        $delivery->order->status = OrderStatusEnum::DONE;
        $delivery->order->save();
        OrderStatusUpdated::dispatch($delivery->order);
    }
}
