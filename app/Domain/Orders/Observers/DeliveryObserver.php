<?php

namespace App\Domain\Orders\Observers;

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Orders\Models\Delivery;

class DeliveryObserver
{
    public function __construct(
        protected SendDeliveryEventAction $sendDeliveryEventAction
    ) {
    }

    public function saving(Delivery $delivery)
    {
        $this->setStatusAt($delivery);
    }

    public function updated(Delivery $delivery)
    {
        $this->sendDeliveryEventAction->execute($delivery, ModelEventMessage::UPDATE);
    }

    /**
     * Установить дату изменения статуса отправления.
     * @param  Delivery  $delivery
     */
    protected function setStatusAt(Delivery $delivery): void
    {
        if ($delivery->isDirty('status')) {
            $delivery->status_at = now();
        }
    }
}
