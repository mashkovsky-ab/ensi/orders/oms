<?php

namespace App\Domain\Orders\Observers;

use App\Domain\Orders\Actions\Shipment\CalcShipmentAction;
use App\Domain\Orders\Actions\Shipment\CalcShipmentCostAction;
use App\Domain\Orders\Actions\Shipment\CalcShipmentSizesAction;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;

class OrderItemObserver
{
    public function __construct(
        protected CalcShipmentSizesAction $calcShipmentSizesAction,
        protected CalcShipmentCostAction $calcShipmentCostAction,
        protected CalcShipmentAction $calcShipmentAction,
    ) {
    }

    public function saving(OrderItem $orderItem)
    {
        $this->setPrice($orderItem);
        $this->setCost($orderItem);
    }

    /**
     * Установить цену элемента корзины со скидкой
     * @param OrderItem $orderItem
     */
    protected function setPrice(OrderItem $orderItem): void
    {
        if ($orderItem->isDirty('price_per_one') || $orderItem->isDirty('qty')) {
            $orderItem->price = round($orderItem->price_per_one * $orderItem->qty);
        }
    }

    /**
     * Установить стоимость элемента корзины
     * @param OrderItem $orderItem
     */
    protected function setCost(OrderItem $orderItem): void
    {
        if ($orderItem->isDirty('cost_per_one') || $orderItem->isDirty('qty')) {
            $orderItem->cost = round($orderItem->cost_per_one * $orderItem->qty);
        }
    }

//    public function created(OrderItem $shipmentItem)
//    {
//        // todo вынести в экшн изменения состава заказа
//        if (!Order::$isCreating) {
//            $this->calcShipmentAction->execute($shipmentItem->shipment);
//        }
//    }
//
//    public function updated(OrderItem $orderItem)
//    {
//        // todo вынести в экшн изменения состава заказа
//        if (!Order::$isCreating) {
//            if ($orderItem->isDirty('qty')) {
//                $this->calcShipmentSizesAction->execute($orderItem->shipment);
//            }
//
//            // todo вынести в экшн изменения состава заказа
//            if ($orderItem->isDirty('qty') || $orderItem->isDirty('price')) {
//                $this->calcShipmentCostAction->execute($orderItem->shipment);
//            }
//        }
//    }
}
