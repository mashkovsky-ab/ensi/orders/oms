<?php

namespace App\Domain\Orders\Observers;

use App\Domain\Orders\Models\Shipment;

class ShipmentObserver
{
    public function saving(Shipment $shipment)
    {
        $this->setStatusAt($shipment);
    }

    /**
     * Установить дату изменения статуса отгрузки
     * @param  Shipment $shipment
     */
    protected function setStatusAt(Shipment $shipment): void
    {
        if ($shipment->isDirty('status')) {
            $shipment->status_at = now();
        }
    }
}
