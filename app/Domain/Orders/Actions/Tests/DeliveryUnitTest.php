<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Orders\Actions\Delivery\PatchDeliveryAction;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use Illuminate\Support\Facades\Event;
use function Pest\Laravel\assertDatabaseHas;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region PatchDeliveryAction
test("PatchDeliveryAction success", function (array $fields) {
    /** @var IntegrationTestCase $this */
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create(['status' => DeliveryStatusEnum::NEW]);

    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');
    Event::fake();

    resolve(PatchDeliveryAction::class)->execute($delivery->id, $fields);

    if (array_key_exists('status', $fields)) {
        Event::assertDispatched(DeliveryStatusUpdated::class);
    } else {
        Event::assertNotDispatched(DeliveryStatusUpdated::class);
    }

    assertDatabaseHas((new Delivery())->getTable(), array_merge([
        'id' => $delivery->id,
    ], $fields));
})->with([
    'with status' => [['status' => DeliveryStatusEnum::ASSEMBLED]],
    'without status' => [['date' => '2021-05-06']],
]);
// endregion
