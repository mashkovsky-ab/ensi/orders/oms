<?php

use App\Domain\Orders\Actions\Shipment\PatchShipmentAction;
use App\Domain\Orders\Events\ShipmentStatusUpdated;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Illuminate\Support\Facades\Event;
use function Pest\Laravel\assertDatabaseHas;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region PatchShipmentAction
test("PatchShipmentAction success", function (array $fields) {
    /** @var IntegrationTestCase $this */
    /** @var Shipment $shipment */
    $shipment = Shipment::factory()->create(['status' => ShipmentStatusEnum::NEW]);
    Event::fake();

    resolve(PatchShipmentAction::class)->execute($shipment->id, $fields);

    if (array_key_exists('status', $fields)) {
        Event::assertDispatched(ShipmentStatusUpdated::class);
    } else {
        Event::assertNotDispatched(ShipmentStatusUpdated::class);
    }

    assertDatabaseHas((new Shipment())->getTable(), array_merge([
        'id' => $shipment->id,
    ], $fields));
})->with([
    'with status' => [['status' => ShipmentStatusEnum::CANCELED]],
    'without status' => [[]],
]);
// endregion
