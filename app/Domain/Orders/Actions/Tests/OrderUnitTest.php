<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Actions\Order\PatchOrderAction;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Illuminate\Support\Facades\Event;
use function Pest\Laravel\assertDatabaseHas;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region PatchOrderAction
test("PatchOrderAction success", function (array $fields) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::NEW]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    Event::fake();

    resolve(PatchOrderAction::class)->execute($order->id, $fields);

    if (array_key_exists('status', $fields)) {
        Event::assertDispatched(OrderStatusUpdated::class);
    } else {
        Event::assertNotDispatched(OrderStatusUpdated::class);
    }

    assertDatabaseHas((new Order())->getTable(), array_merge([
        'id' => $order->id,
    ], $fields));
})->with([
    'with status' => [['status' => OrderStatusEnum::APPROVED]],
    'without status' => [['responsible_id' => 1]],
]);

test("PatchOrderAction cancel success", function (int $deliveryStatus, int $checkOrderStatus, int $checkDeliveryStatus, int $checkShipmentStatus) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::APPROVED]);
    Delivery::factory()->for($order)->create(['status' => DeliveryStatusEnum::ASSEMBLED]);
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->for($order)->create(['status' => $deliveryStatus]);
    Shipment::factory()->for($delivery)->create(['status' => ShipmentStatusEnum::ASSEMBLED]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');

    try {
        resolve(PatchOrderAction::class)->execute($order->id, ['status' => OrderStatusEnum::CANCELED]);
    } catch (\Exception) {
    }

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'status' => $checkOrderStatus,
    ]);
    assertDatabaseHas((new Delivery())->getTable(), [
        'order_id' => $order->id,
        'status' => $checkDeliveryStatus,
    ]);
    assertDatabaseHas((new Shipment())->getTable(), [
        'delivery_id' => $delivery->id,
        'status' => $checkShipmentStatus,
    ]);
})->with([
    'can change' => [DeliveryStatusEnum::ASSEMBLED, OrderStatusEnum::CANCELED, DeliveryStatusEnum::CANCELED, ShipmentStatusEnum::CANCELED],
    'can\'t change' => [DeliveryStatusEnum::DONE, OrderStatusEnum::APPROVED, DeliveryStatusEnum::ASSEMBLED, ShipmentStatusEnum::ASSEMBLED],
]);
// endregion
