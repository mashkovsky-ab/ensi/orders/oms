<?php

namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\PaymentData;
use App\Domain\PaymentSystems\Systems\ReceiptItemData;

class MakePaymentDataAction
{
    public function execute(Order $order, bool $withOrderInfo = false): PaymentData
    {
        $paymentDataClass = PaymentData::getClassBySystemId($order->payment_system);

        /** @var PaymentData $paymentData */
        $paymentData = new $paymentDataClass($order->payment_data, $order->price);
        $paymentData->status = $order->payment_status;
        $paymentData->paymentLink = $order->payment_link;
        $paymentData->externalId = $order->payment_external_id;
        $paymentData->expiresAt = $order->payment_expires_at;
        $paymentData->payedAt = $order->payed_at;

        if ($withOrderInfo) {
            $paymentData->orderId = $order->id;
            $paymentData->customerEmail = $order->customer_email;
            foreach ($order->items as $item) {
                $receiptItemData = new ReceiptItemData();
                $receiptItemData->name = $item->name;
                $receiptItemData->qty = $item->qty;
                $receiptItemData->price = $item->price;
                $paymentData->receiptItems[] = $receiptItemData;
            }
        }

        return $paymentData;
    }
}
