<?php

namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\PaymentData;

class SaveFromPaymentDataAction
{
    public function __construct(protected SetStatusPaymentAction $setStatusPaymentAction)
    {
    }

    public function execute(Order $order, PaymentData $paymentData)
    {
        $order->payment_data = $paymentData->getData();
        $order->payment_link = $paymentData->paymentLink;
        $order->payment_external_id = $paymentData->externalId;
        $order->payment_expires_at = $paymentData->expiresAt;
        $order->payed_at = $paymentData->payedAt;
        // Сами не делаем сохранение, т.к. сохранится в экшене SetStatusPaymentAction
        $this->setStatusPaymentAction->execute($order, $paymentData->status);
    }
}
