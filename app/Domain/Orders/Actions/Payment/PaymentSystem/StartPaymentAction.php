<?php


namespace App\Domain\Orders\Actions\Payment\PaymentSystem;

use App\Domain\Common\Models\Setting;
use App\Domain\Orders\Actions\Payment\MakePaymentDataAction;
use App\Domain\Orders\Actions\Payment\SaveFromPaymentDataAction;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Carbon\Carbon;

class StartPaymentAction
{
    public function __construct(
        protected MakePaymentDataAction $makePaymentDataAction,
        protected SaveFromPaymentDataAction $savePaymentAction,
    ) {
    }

    public function execute(Order $order, string $returnUrl): string
    {
        if ($order->payment_status != PaymentStatusEnum::NOT_PAID) {
            throw new ValidateException('Статус оплаты не позволяет начать оплату');
        }
        if ($order->payment_method == PaymentMethodEnum::OFFLINE) {
            throw new ValidateException('Способ оплаты не позволяет начать оплату');
        }

        if (!$order->payment_link) {
            $minutes = Setting::getValue(SettingCodeEnum::PAYMENT);
            if ($minutes) {
                $order->payment_expires_at = Carbon::now()->addMinutes($minutes);
                $order->save();
            }

            $paymentData = $this->makePaymentDataAction->execute($order, true);
            $order->paymentSystem()->createExternalPayment($paymentData, $returnUrl);
            $this->savePaymentAction->execute($order, $paymentData);
        }

        return $order->payment_link;
    }
}
