<?php


namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Actions\Payment\PaymentSystem\CommitPaymentAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Illuminate\Support\Collection;

class CommitOldPaymentsAction
{
    public function __construct(protected CommitPaymentAction $commitPaymentAction)
    {
    }

    public function execute()
    {
        /** @var Collection|Order[] $orders */
        $orders = Order::query()
            ->where('payment_status', PaymentStatusEnum::HOLD)
            ->get();
        $threeDaysAgo = now()->subDays(3);
        foreach ($orders as $order) {
            try {
                if ($threeDaysAgo->greaterThan($order->created_at)) {
                    $this->commitPaymentAction->execute($order);
                }
            } catch (\Exception $e) {
                // Ловим исключение что бы в случае падения одной оплаты остальные не блокировались
            }
        }
    }
}
