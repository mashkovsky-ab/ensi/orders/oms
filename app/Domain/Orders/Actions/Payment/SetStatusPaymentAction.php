<?php

namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;

class SetStatusPaymentAction
{
    public function execute(Order $order, int $status): void
    {
        $order->payment_status = $status;
        switch ($status) {
            case PaymentStatusEnum::TIMEOUT:
                $order->status = OrderStatusEnum::CANCELED;

                break;
            case PaymentStatusEnum::PAID:
                $order->status = OrderStatusEnum::NEW;

                break;
        }

        if ($order->isDirty()) {
            $order->save();
            OrderStatusUpdated::dispatch($order);
        }
    }
}
