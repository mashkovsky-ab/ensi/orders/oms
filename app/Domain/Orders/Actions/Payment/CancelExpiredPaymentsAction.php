<?php


namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class CancelExpiredPaymentsAction
{
    public function __construct(protected SetStatusPaymentAction $setTimeoutPaymentAction)
    {
    }

    public function execute(): void
    {
        /** @var Collection|Order[] $orders */
        $orders = Order::query()
            ->where('payment_status', PaymentStatusEnum::NOT_PAID)
            ->where('payment_expires_at', '<', Carbon::now()->format('Y-m-d H:i:s'))
            ->get();
        foreach ($orders as $order) {
            $this->setTimeoutPaymentAction->execute($order, PaymentStatusEnum::TIMEOUT);
        }
    }
}
