<?php

namespace App\Domain\Orders\Actions\OrderItem;

use App\Domain\Orders\Actions\Shipment\CalcShipmentAction;
use App\Domain\Orders\Models\OrderItem;

class DeleteOrderItemAction
{
    public function __construct(protected CalcShipmentAction $calcShipmentAction)
    {
    }

    public function execute(OrderItem $orderItem)
    {
        $orderItem->delete();
        $this->calcShipmentAction->execute($orderItem->shipment);
    }
}
