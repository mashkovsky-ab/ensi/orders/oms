<?php

namespace App\Domain\Orders\Actions\Delivery;

use App\Domain\Orders\Actions\CalcModelSizesAction;
use App\Domain\Orders\Models\Delivery;

class CalcDeliverySizesAction extends CalcModelSizesAction
{
    protected Delivery $delivery;

    public function execute(Delivery $delivery)
    {
        $this->delivery = $delivery;
        $this->fillSizes();
        $this->delivery->save();
    }

    protected function calcVolume(): void
    {
        foreach ($this->delivery->shipments as $shipment) {
            $this->volume += $shipment->width * $shipment->height * $shipment->length;
        }
    }

    protected function calcMaxSide(): void
    {
        foreach ($this->delivery->shipments as $shipment) {
            $this->checkMaxSize($shipment->weight, static::MAX_WIDTH);
            $this->checkMaxSize($shipment->height, static::MAX_HEIGHT);
            $this->checkMaxSize($shipment->length, static::MAX_LENGTH);
        }
    }

    protected function setWeight(): void
    {
        $weight = 0;

        foreach ($this->delivery->shipments as $shipment) {
            $weight += $shipment->weight;
        }

        $this->delivery->weight = $weight;
    }

    protected function setWidth(float $width): void
    {
        $this->delivery->width = $width;
    }

    protected function setHeight(float $height): void
    {
        $this->delivery->height = $height;
    }

    protected function setLength(float $length): void
    {
        $this->delivery->length = $length;
    }
}
