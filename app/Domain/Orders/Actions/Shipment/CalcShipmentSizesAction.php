<?php

namespace App\Domain\Orders\Actions\Shipment;

use App\Domain\Orders\Actions\CalcModelSizesAction;
use App\Domain\Orders\Actions\Delivery\CalcDeliverySizesAction;
use App\Domain\Orders\Models\Shipment;

class CalcShipmentSizesAction extends CalcModelSizesAction
{
    protected Shipment $shipment;

    public function __construct(
        protected CalcDeliverySizesAction $calcDeliverySizesAction
    ) {
    }

    public function execute(Shipment $shipment)
    {
        $this->shipment = $shipment;
        $this->fillSizes();

        if ($shipment->isDirty(['weight', 'width', 'height', 'length'])) {
            $this->shipment->save();
            $this->calcDeliverySizesAction->execute($shipment->delivery);
        }
    }

    protected function calcVolume(): void
    {
        $this->shipment->loadMissing(['orderItems']);

        foreach ($this->shipment->orderItems as $orderItem) {
            $this->volume += $orderItem->product_width * $orderItem->product_height * $orderItem->product_length * $orderItem->qty;
        }
    }

    protected function calcMaxSide(): void
    {
        $this->shipment->loadMissing(['orderItems']);

        foreach ($this->shipment->orderItems as $orderItem) {
            $this->checkMaxSize($orderItem->product_width, static::MAX_WIDTH);
            $this->checkMaxSize($orderItem->product_height, static::MAX_HEIGHT);
            $this->checkMaxSize($orderItem->product_length, static::MAX_LENGTH);
        }
    }

    protected function setWeight(): void
    {
        $weight = 0;
        $this->shipment->loadMissing(['orderItems']);

        foreach ($this->shipment->orderItems as $orderItem) {
            $weight += $orderItem->product_weight * $orderItem->qty;
        }

        $this->shipment->weight = $weight;
    }

    protected function setWidth(float $width): void
    {
        $this->shipment->width = $width;
    }

    protected function setHeight(float $height): void
    {
        $this->shipment->height = $height;
    }

    protected function setLength(float $length): void
    {
        $this->shipment->length = $length;
    }
}
