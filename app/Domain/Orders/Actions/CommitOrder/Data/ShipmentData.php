<?php

namespace App\Domain\Orders\Actions\CommitOrder\Data;

class ShipmentData
{
    /** @var int */
    public $sellerId;
    /** @var int */
    public $storeId;
    /** @var OrderItemData[] */
    public $items;
}
