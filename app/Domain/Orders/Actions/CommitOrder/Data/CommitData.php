<?php


namespace App\Domain\Orders\Actions\CommitOrder\Data;

use App\Domain\Orders\Data\DeliveryAddress;
use Illuminate\Support\Collection;

class CommitData
{
    public int $customerId;
    public string $customerEmail;
    public ?string $clientComment;

    public int $source;
    public int $paymentSystemId;
    public ?int $spentBonus;
    public ?int $addedBonus;
    public ?string $promoCode;

    public int $deliveryService;
    public int $deliveryMethod;
    public int $deliveryTariffId;
    public ?int $deliveryPointId;
    public ?DeliveryAddress $deliveryAddress = null;
    public ?string $deliveryComment;
    public int $deliveryCost;
    public int $deliveryPrice;

    public string $receiverName;
    public string $receiverPhone;
    public string $receiverEmail;

    /** @var Collection|DeliveryData[] */
    public Collection $deliveries;

    public function price(): float
    {
        $itemsPrice = $this->items()->sum(fn (OrderItemData $item) => ($item->pricePerOne * $item->qty));

        return round($itemsPrice) + $this->deliveryPrice;
    }

    public function cost(): float
    {
        $itemsCost = $this->items()->sum(fn (OrderItemData $item) => ($item->costPerOne * $item->qty));

        return round($itemsCost) + $this->deliveryCost;
    }

    /**
     * @return Collection|OrderItemData[]
     */
    public function items(): Collection
    {
        return $this->deliveries
            ->pluck('shipments')
            ->collapse()
            ->pluck('items')
            ->collapse();
    }
}
