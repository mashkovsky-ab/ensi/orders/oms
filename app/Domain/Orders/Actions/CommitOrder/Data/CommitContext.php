<?php


namespace App\Domain\Orders\Actions\CommitOrder\Data;

use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use Ensi\OffersClient\Dto\ReserveStocksRequestItems;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class CommitContext
{
    public LoggerInterface $logger;
    protected Order $order;
    /** @var ProductInfoData[] */
    protected array $products = [];

    /** @var Collection|ReserveStocksRequestItems[] */
    protected $reserveItems;

    public function __construct(public CommitData $data)
    {
        $this->reserveItems = collect();
        $this->logger = Log::channel('checkout');
    }

    public function putOrder(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function getReserveItems(): Collection
    {
        return $this->reserveItems;
    }

    public function addReserveItem(OrderItem $orderItem, int $storeId)
    {
        $reserveItem = (new ReserveStocksRequestItems())
            ->setOfferId($orderItem->offer_id)
            ->setQty($orderItem->qty)
            ->setStoreId($storeId);
        $this->reserveItems->push($reserveItem);
    }

    public function getProductInfo(int $offerId): ProductInfoData
    {
        return $this->products[$offerId];
    }

    public function setProductInfo(int $offerId, ProductInfoData $data)
    {
        $this->products[$offerId] = $data;
    }
}
