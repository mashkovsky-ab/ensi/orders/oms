<?php

namespace App\Domain\Orders\Actions\CommitOrder\Data;

use App\Domain\Orders\Data\Timeslot;
use Carbon\Carbon;

class DeliveryData
{
    public Carbon $date;
    public ?Timeslot $timeslot = null;
    public int $cost;
    /** @var ShipmentData[] */
    public array $shipments;
}
