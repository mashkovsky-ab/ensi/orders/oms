<?php

namespace App\Domain\Orders\Actions\CommitOrder\Data;

class OrderItemData
{
    public int $offerId;
    public int $costPerOne;
    public int $pricePerOne;
    public float $qty;
}
