<?php


namespace App\Domain\Orders\Actions\CommitOrder\Stages;

use App\Domain\Orders\Actions\CommitOrder\Data\CommitContext;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;

class CreateOrderAction
{
    public function execute(CommitContext $context): void
    {
        $context->logger->info('Start create order record');

        $order = new Order();
        $paymentMethodId = PaymentSystem::getMethodById($context->data->paymentSystemId);
        $order->status = $paymentMethodId == PaymentMethodEnum::OFFLINE ? OrderStatusEnum::NEW : OrderStatusEnum::WAIT_PAY;
        $order->source = $context->data->source;
        $order->customer_id = $context->data->customerId;
        $order->customer_email = $context->data->customerEmail;
        $order->client_comment = $context->data->clientComment;
        $order->cost = $context->data->cost();
        $order->price = $context->data->price();
        $order->spent_bonus = $context->data->spentBonus ?: 0;
        $order->added_bonus = $context->data->addedBonus ?: 0;
        $order->promo_code = $context->data->promoCode;

        $order->delivery_service = $context->data->deliveryService;
        $order->delivery_method = $context->data->deliveryMethod;
        $order->delivery_tariff_id = $context->data->deliveryTariffId;
        $order->delivery_point_id = $context->data->deliveryPointId;
        $order->delivery_address = $context->data->deliveryAddress;
        $order->delivery_comment = $context->data->deliveryComment;
        $order->delivery_cost = $context->data->deliveryCost;
        $order->delivery_price = $context->data->deliveryPrice;

        $order->receiver_name = $context->data->receiverName;
        $order->receiver_email = $context->data->receiverEmail;
        $order->receiver_phone = $context->data->receiverPhone;

        $order->payment_system = $context->data->paymentSystemId;
        $order->payment_status = PaymentStatusEnum::NOT_PAID;

        $order->save();

        $order->number = $this->generateOrderNumber($order);
        $order->saveQuietly();

        $context->putOrder($order);
    }

    protected function generateOrderNumber(Order $order): string
    {
        return $order->id + 1000000;
    }
}
