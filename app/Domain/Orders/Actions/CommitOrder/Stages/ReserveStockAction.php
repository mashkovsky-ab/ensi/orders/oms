<?php


namespace App\Domain\Orders\Actions\CommitOrder\Stages;

use App\Domain\Orders\Actions\CommitOrder\Data\CommitContext;
use Ensi\OffersClient\Api\StocksApi;
use Ensi\OffersClient\Dto\ReserveStocksRequest;

class ReserveStockAction
{
    public function __construct(protected StocksApi $stocksApi)
    {
    }

    public function execute(CommitContext $context): void
    {
        $reserveByOrderItems = $context->getReserveItems();
        // Уменьшаем остатки
        if ($reserveByOrderItems->isNotEmpty()) {
            try {
                $this->stocksApi->reserveStocks(
                    (new ReserveStocksRequest())->setItems($reserveByOrderItems->all())
                );
            } catch (\Exception $e) {
                $context->logger->error($e->getMessage(), [
                    'code' => $e->getCode(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString(),
                ]);

                throw $e;
            }
        }
    }
}
