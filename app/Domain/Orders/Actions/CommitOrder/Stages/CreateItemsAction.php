<?php


namespace App\Domain\Orders\Actions\CommitOrder\Stages;

use App\Domain\Orders\Actions\CommitOrder\Data\CommitContext;
use App\Domain\Orders\Actions\Shipment\CalcShipmentAction;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Illuminate\Database\Eloquent\Collection;

class CreateItemsAction
{
    public function __construct(
        protected CalcShipmentAction $calcShipmentAction,
    ) {
    }

    public function execute(CommitContext $context): void
    {
        $context->logger->info('Start create shipments');

        $shipmentNumber = 1;
        $order = $context->getOrder();
        $deliveries = new Collection();
        foreach ($context->data->deliveries as $i => $deliveryData) {
            $delivery = new Delivery();
            $delivery->setRelation('order', $order);
            $delivery->order_id = $order->id;
            $delivery->status = DeliveryStatusEnum::NEW;
            $delivery->number = Delivery::makeNumber($order->number, ++$i);

            $delivery->cost = $deliveryData->cost;

            $delivery->date = $deliveryData->date;
            $delivery->timeslot = $deliveryData->timeslot;

            $delivery->save();
            $deliveries->push($delivery);

            foreach ($deliveryData->shipments as $shipmentData) {
                $shipment = new Shipment();
                $shipment->setRelation('delivery', $delivery);
                $shipment->delivery_id = $delivery->id;
                $shipment->status = ShipmentStatusEnum::NEW;
                $shipment->seller_id = $shipmentData->sellerId;
                $shipment->store_id = $shipmentData->storeId;
                $shipment->number = Shipment::makeNumber($order->number, $i, $shipmentNumber++);
                $shipment->save();

                foreach ($shipmentData->items as $itemData) {
                    $productInfo = $context->getProductInfo($itemData->offerId);
                    $orderItem = new OrderItem();
                    $orderItem->setRelation('shipment', $shipment);
                    $orderItem->setRelation('order', $order);
                    $orderItem->offer_id = $itemData->offerId;
                    $orderItem->shipment_id = $shipment->id;
                    $orderItem->order_id = $order->id;
                    $orderItem->qty = $itemData->qty;
                    $orderItem->cost_per_one = $itemData->costPerOne;
                    $orderItem->price_per_one = $itemData->pricePerOne;
                    $orderItem->name = $productInfo->product->getName();
                    $orderItem->product_weight = $productInfo->product->getWeight();
                    $orderItem->product_width = $productInfo->product->getWidth();
                    $orderItem->product_weight_gross = $productInfo->product->getWeightGross();
                    $orderItem->offer_external_id = $productInfo->offer->getExternalId();
                    $orderItem->offer_storage_address = $productInfo->offer->getStorageAddress();
                    $orderItem->product_barcode = $productInfo->product->getBarcode();
                    $orderItem->product_height = $productInfo->product->getHeight();
                    $orderItem->product_length = $productInfo->product->getLength();
                    $orderItem->save();

                    // Заполняем информацию о товарах, которые надо будет зарезервировать
                    $context->addReserveItem($orderItem, $shipmentData->storeId);
                }

                $this->calcShipmentAction->execute($shipment);
            }
        }
        $order->setRelation('deliveries', $deliveries);
    }
}
