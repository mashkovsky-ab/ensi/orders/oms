<?php


namespace App\Domain\Orders\Actions\CommitOrder\Stages;

use App\Domain\Orders\Actions\CommitOrder\Data\CommitContext;
use App\Domain\Orders\Actions\CommitOrder\Data\ProductInfoData;
use App\Exceptions\ValidateException;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OffersClient\Dto\Stock;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Collection;

class LoadProductInfoAction
{
    public function __construct(
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
    ) {
    }

    public function execute(CommitContext $context): void
    {
        $context->logger->info('Start fill product info');

        $items = $context->data->items();
        $offerIds = $items->pluck('offerId')->all();

        $offers = $this->loadOffers($offerIds);
        $products = $this->loadProducts($offers);

        if (!$this->checkProductsActive($products)) {
            throw new ValidateException('В запросе присутствует неактивный товар');
        }

        foreach ($items as $item) {
            $offer = $offers[$item->offerId] ?? null;
            if (!$offer) {
                throw new ValidateException("Для товара {$item->offerId} не найден оффер");
            }
            $product = $products[$offer->getProductId()] ?? null;
            if (!$product) {
                throw new ValidateException("Для товара {$item->offerId} не найден товар");
            }
            /** @var Stock $stock */
            $stock = collect($offer->getStocks())->first();
            if (!$stock) {
                throw new ValidateException("Для товара {$item->offerId} не найден сток");
            }
            if ($stock->getQty() < $item->qty) {
                throw new ValidateException("Кол-ва товара {$item->offerId} недостаточно на складе");
            }

            $productInfo = new ProductInfoData();
            $productInfo->offer = $offer;
            $productInfo->product = $product;

            $context->setProductInfo($item->offerId, $productInfo);
        }
    }

    /**
     * @param Collection<int,Product> $products
     * @return bool
     */
    protected function checkProductsActive(Collection $products): bool
    {
        return $products->every(fn (Product $product) => $product->getAllowPublish() === true);
    }

    /**
     * @param int[] $offerIds
     * @return Collection<int,Offer>
     */
    protected function loadOffers(array $offerIds): Collection
    {
        $searchOffersRequest = new SearchOffersRequest();
        $searchOffersRequest->setFilter((object)[
            'id' => $offerIds,
        ]);
        $searchOffersRequest->setInclude(['stocks']);

        return collect($this->offersApi->searchOffers($searchOffersRequest)->getData())->keyBy('id');
    }

    /**
     * @param Collection<int,Offer> $offers
     * @return Collection<int,Product>
     */
    protected function loadProducts(Collection $offers): Collection
    {
        $searchProductsRequest = new SearchProductsRequest();
        $searchProductsRequest->setFilter((object)[
            'id' => $offers->pluck('product_id')->all(),
        ]);

        return collect($this->productsApi->searchProducts($searchProductsRequest)->getData())->keyBy('id');
    }
}
