<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use Illuminate\Support\Facades\DB;

class PatchOrderAction
{
    public function execute(int $orderId, array $fields): Order
    {
        /** @var Order $order */
        $order = Order::query()->findOrFail($orderId);
        if (isset($fields['client_comment']) && !in_array($order->status, OrderStatus::waitProcess())) {
            throw new ValidateException("Нельзя обновить комментарий клиента в текущем статусе");
        }

        $order->fill($fields);
        DB::transaction(function () use ($order) {
            $order->save();
            if ($order->wasChanged('status')) {
                OrderStatusUpdated::dispatch($order);
            }
        });

        $order->setRelations([]);

        return $order;
    }
}
