<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Actions\Payment\PaymentSystem\ReinitPaymentAction;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;

class ChangeOrderPaymentSystemAction
{
    public function __construct(protected ReinitPaymentAction $reinitPaymentAction)
    {
    }

    public function execute(Order $order, int $paymentSystem): Order
    {
        if ($order->isPaid()) {
            throw new ValidateException("Нельзя изменить систему оплаты для оплаченного заказа");
        }

        if (!in_array($order->status, OrderStatus::waitProcess())) {
            throw new ValidateException("Нельзя изменить систему оплаты в текущем статусе заказа");
        }

        if ($order->payment_system == $paymentSystem) {
            return $order;
        }

        $order->payment_system = $paymentSystem;
        $order->payment_status = PaymentStatusEnum::NOT_PAID;
        $order->payment_data = [];
        $order->payment_link = null;
        $order->payment_external_id = null;
        $order->payment_expires_at = null;
        $order->payed_at = null;
        $order->save();

        $this->reinitPaymentAction->execute($order);

        return $order;
    }
}
