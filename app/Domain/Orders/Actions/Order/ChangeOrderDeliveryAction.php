<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Events\OrderPricesUpdated;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;

class ChangeOrderDeliveryAction
{
    public function execute(int $orderId, array $fields): Order
    {
        /** @var Order $order */
        $order = Order::query()->findOrFail($orderId);

        if ($order->isPaid()) {
            throw new ValidateException("Нельзя обновить данные по доставке для оплаченного заказа");
        }

        if (!in_array($order->status, OrderStatus::waitProcess())) {
            throw new ValidateException("Нельзя обновить данные по доставке в текущем статусе заказа");
        }

        $order->update($fields);

        if ($order->wasChanged(['delivery_cost', 'delivery_price'])) {
            OrderPricesUpdated::dispatch($order);
        }

        $order->setRelations([]);

        return $order;
    }
}
