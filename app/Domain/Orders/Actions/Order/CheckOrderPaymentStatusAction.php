<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Actions\Payment\PaymentSystem\CheckPaymentAction;
use App\Domain\Orders\Models\Order;

class CheckOrderPaymentStatusAction
{
    public function __construct(protected CheckPaymentAction $checkPaymentAction)
    {
    }

    public function execute(int $orderId): int
    {
        /** @var Order $order */
        $order = Order::query()->where('id', $orderId)->firstOrFail();
        $this->checkPaymentAction->execute($order);

        return $order->payment_status;
    }
}
