<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Models\OrderFile;
use App\Exceptions\ValidateException;
use Illuminate\Database\Eloquent\Collection;

class DeleteOrderFilesAction
{
    public function execute(int $orderId, array $ids): void
    {
        /** @var Collection|OrderFile[] $files */
        $files = OrderFile::query()->whereIn('id', $ids)->get();

        $badCount = $files->filter(fn (OrderFile $file) => $file->order_id != $orderId)->count();
        if ($badCount > 0) {
            throw new ValidateException("Не все файлы относятся к указанному заказу");
        }

        foreach ($files as $file) {
            $file->delete();
        }
    }
}
