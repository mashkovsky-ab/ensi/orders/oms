<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Common\Models\Setting;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class MarkExpiredOrdersAction
{
    public function execute(): void
    {
        $minutes = Setting::getValue(SettingCodeEnum::PROCESSING);

        /** @var Collection|Order[] $orders */
        $orders = Order::query()
            ->where('status', OrderStatusEnum::NEW)
            ->where('created_at', '<', Carbon::now()->subMinutes($minutes)->format('Y-m-d H:i:s'))
            ->get();

        foreach ($orders as $order) {
            $order->is_expired = true;
            $order->save();
        }
    }
}
