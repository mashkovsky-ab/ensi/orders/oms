<?php


namespace App\Domain\Orders\Data;

use App\Domain\Orders\Data\Tests\Factories\TimeslotFactory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;

/**
 * Class Timeslot
 * @package App\Domain\Orders\Data
 * @property string $id
 * @property string $from
 * @property string $to
 */
class Timeslot extends Fluent
{
    public static function rules(): array
    {
        return [
            "id" => "required|string",
            "from" => "required|string",
            "to" => "required|string",
        ];
    }

    public static function factory(): TimeslotFactory
    {
        return TimeslotFactory::new();
    }

    public static function validateOrFail(array $timeslot): self
    {
        $validator = Validator::make($timeslot, self::rules());

        return new self($validator->validate());
    }
}
