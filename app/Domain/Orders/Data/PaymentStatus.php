<?php


namespace App\Domain\Orders\Data;

use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;

class PaymentStatus
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            PaymentStatusEnum::NOT_PAID => 'Не оплачен',
            PaymentStatusEnum::HOLD => 'Средства захолдированы',
            PaymentStatusEnum::PAID => 'Оплачен',
            PaymentStatusEnum::RETURNED => 'Возврат',
            PaymentStatusEnum::TIMEOUT => 'Просрочен',
            PaymentStatusEnum::CANCELED => 'Отменен',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];
        foreach (PaymentStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }

    public static function isPaid(int $paymentStatus): bool
    {
        return in_array($paymentStatus, [PaymentStatusEnum::PAID, PaymentStatusEnum::HOLD]);
    }

    public static function getAllowedNext(Order $order): array
    {
        return match ($order->payment_status) {
            PaymentStatusEnum::NOT_PAID => [PaymentStatusEnum::HOLD, PaymentStatusEnum::PAID, PaymentStatusEnum::TIMEOUT, PaymentStatusEnum::CANCELED],
            PaymentStatusEnum::HOLD => [PaymentStatusEnum::PAID, PaymentStatusEnum::RETURNED],
            PaymentStatusEnum::PAID => [PaymentStatusEnum::RETURNED],
//            PaymentStatusEnum::RETURNED => [],
//            PaymentStatusEnum::TIMEOUT => [],
//            PaymentStatusEnum::CANCELED => [],
            default => [],
        };
    }
}
