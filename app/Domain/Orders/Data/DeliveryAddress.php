<?php


namespace App\Domain\Orders\Data;

use App\Domain\Orders\Data\Tests\Factories\DeliveryAddressFactory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;

/**
 * Class DeliveryAddress
 * @package App\Domain\Orders\Data
 * @property string $address_string
 * @property string $country_code
 * @property string $post_index
 * @property string $region
 * @property string $region_guid
 * @property string $area
 * @property string $area_guid
 * @property string $city
 * @property string $city_guid
 * @property string $street
 * @property string $house
 * @property string $block
 * @property string $flat
 * @property string $floor
 * @property string $porch
 * @property string $intercom
 * @property string $geo_lat
 * @property string $geo_lon
 */
class DeliveryAddress extends Fluent
{
    public static function rules(): array
    {
        return [
            "address_string" => "required|string",
            "country_code" => 'nullable|string',
            "post_index" => 'nullable|string',
            "region" => 'nullable|string',
            "region_guid" => 'nullable|string',
            "area" => 'nullable|string',
            "area_guid" => 'nullable|string',
            "city" => 'nullable|string',
            "city_guid" => 'nullable|string',
            "street" => 'nullable|string',
            "house" => 'nullable|string',
            "block" => 'nullable|string',
            "flat" => 'nullable|string',
            "floor" => 'nullable|string',
            "porch" => 'nullable|string',
            "intercom" => 'nullable|string',
            "geo_lat" => "required|string",
            "geo_lon" => "required|string",
        ];
    }

    public static function validateOrFail(array $deliveryAddress): self
    {
        $validator = Validator::make($deliveryAddress, self::rules());

        return new self($validator->validate());
    }

    /**
     * Формирует строку с полным адресом.
     *
     * @param bool $addNewLine Добавить перенос строки перед домофоном (если он задан)
     * @return string
     */
    public function getFullString(bool $addNewLine = false): string
    {
        $addressParts = [
            $this->address_string ?? '',
            $this->porch ? 'под. ' . $this->porch : null,
            $this->floor ? 'эт. ' . $this->floor : null,
            $this->flat ? 'кв. ' . $this->flat : null,
        ];

        $intercomLine = $this->intercom ? 'домофон ' . $this->intercom : null;
        if ($intercomLine) {
            $intercomLine = ($addNewLine ? "\n" : '') . $intercomLine;
        }
        $addressParts[] = $intercomLine;

        return join(', ', array_filter($addressParts));
    }

    public static function factory(): DeliveryAddressFactory
    {
        return DeliveryAddressFactory::new();
    }
}
