<?php


namespace App\Domain\Orders\Data;

use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;

class ShipmentStatus
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            ShipmentStatusEnum::NEW => 'Новый',
            ShipmentStatusEnum::IN_WORK => 'В работе',
            ShipmentStatusEnum::ASSEMBLED => 'Собран',
            ShipmentStatusEnum::CANCELED => 'Отменен',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];
        foreach (ShipmentStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }

    public static function cancelled(): array
    {
        return [
            ShipmentStatusEnum::CANCELED,
        ];
    }

    public static function assembled(): array
    {
        return [
            ShipmentStatusEnum::ASSEMBLED,
        ];
    }

    public static function getAllowedNext(Shipment $shipment): array
    {
        return match ($shipment->status) {
            ShipmentStatusEnum::NEW => [ShipmentStatusEnum::IN_WORK, ShipmentStatusEnum::CANCELED],
            ShipmentStatusEnum::IN_WORK => [ShipmentStatusEnum::ASSEMBLED, ShipmentStatusEnum::CANCELED],
            ShipmentStatusEnum::ASSEMBLED => [ShipmentStatusEnum::CANCELED],
//            ShipmentStatusEnum::CANCELED => [],
            default => [],
        };
    }
}
