<?php


namespace App\Domain\Orders\Data;

use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;

class DeliveryStatus
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            DeliveryStatusEnum::NEW => 'Новый',
            DeliveryStatusEnum::ASSEMBLED => 'Собран',
            DeliveryStatusEnum::TRANSFER => 'Передан СД',
            DeliveryStatusEnum::READY_TO_PICKUP => 'Готов к выдаче',
            DeliveryStatusEnum::DONE => 'Получен',
            DeliveryStatusEnum::CANCELED => 'Отменен',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];
        foreach (DeliveryStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }

    public static function cancelled(): array
    {
        return [
            DeliveryStatusEnum::CANCELED,
        ];
    }

    public static function assembled(): array
    {
        return array_merge([
            DeliveryStatusEnum::ASSEMBLED,
            DeliveryStatusEnum::TRANSFER,
            DeliveryStatusEnum::READY_TO_PICKUP,
        ], self::done());
    }

    public static function done(): array
    {
        return [
            DeliveryStatusEnum::DONE,
        ];
    }

    public static function getAllowedNext(Delivery $delivery): array
    {
        return match ($delivery->status) {
            DeliveryStatusEnum::NEW => [DeliveryStatusEnum::ASSEMBLED, DeliveryStatusEnum::CANCELED],
            DeliveryStatusEnum::ASSEMBLED => [DeliveryStatusEnum::TRANSFER, DeliveryStatusEnum::CANCELED],
            DeliveryStatusEnum::TRANSFER => [DeliveryStatusEnum::DONE, DeliveryStatusEnum::READY_TO_PICKUP, DeliveryStatusEnum::CANCELED],
            DeliveryStatusEnum::READY_TO_PICKUP => [DeliveryStatusEnum::DONE, DeliveryStatusEnum::CANCELED],
//            DeliveryStatusEnum::DONE => [],
//            DeliveryStatusEnum::CANCELED => [],
            default => [],
        };
    }
}
