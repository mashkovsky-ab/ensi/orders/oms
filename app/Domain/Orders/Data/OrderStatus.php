<?php


namespace App\Domain\Orders\Data;

use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;

class OrderStatus
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            OrderStatusEnum::WAIT_PAY => 'Ожидает оплаты',
            OrderStatusEnum::NEW => 'Новый',
            OrderStatusEnum::APPROVED => 'Подтвержден',
            OrderStatusEnum::DONE => 'Завершён',
            OrderStatusEnum::CANCELED => 'Отменен',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];
        foreach (OrderStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }

    public static function cancelled(): array
    {
        return [
            OrderStatusEnum::CANCELED,
        ];
    }

    public static function waitProcess(): array
    {
        return [
            OrderStatusEnum::WAIT_PAY,
            OrderStatusEnum::NEW,
        ];
    }

    public static function done(): array
    {
        return [
            OrderStatusEnum::DONE,
        ];
    }

    public static function getAllowedNext(Order $order): array
    {
        return match ($order->status) {
            OrderStatusEnum::WAIT_PAY => [OrderStatusEnum::NEW, OrderStatusEnum::CANCELED],
            OrderStatusEnum::NEW => [OrderStatusEnum::APPROVED, OrderStatusEnum::CANCELED],
            OrderStatusEnum::APPROVED => [OrderStatusEnum::DONE, OrderStatusEnum::CANCELED],
//            OrderStatusEnum::DONE => [],
//            OrderStatusEnum::CANCELED => [],
            default => [],
        };
    }
}
