<?php

use App\Domain\Kafka\Actions\Listen\ListenDeliveryOrderAction;
use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Messages\Listen\DeliveryOrder\DeliveryOrderEventMessage;
use App\Domain\Kafka\Messages\Listen\Tests\Factories\DeliveryOrderEventMessageFactory;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use Illuminate\Support\Facades\Event;
use function Pest\Laravel\assertDatabaseHas;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region ListenDeliveryOrderAction
test("Action ListenDeliveryOrderAction success", function (string $event, int $status, ?array $dirty, bool $change) {
    /** @var IntegrationTestCase $this */
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create(['status' => DeliveryStatusEnum::ASSEMBLED]);
    $message = DeliveryOrderEventMessageFactory::new()->make([
        'event' => $event,
        'attributes' => [
            'delivery_id' => $delivery->id,
            'status' => $status,
        ],
        'dirty' => $dirty,
    ]);

    if ($change) {
        $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');
        Event::fake();
    }

    resolve(ListenDeliveryOrderAction::class)->execute($message);

    if ($change) {
        Event::assertDispatched(DeliveryStatusUpdated::class);
    }

    assertDatabaseHas((new Delivery())->getTable(), [
        'id' => $delivery->id,
        'status' => $change ? $status : $delivery->status,
    ]);
})->with([
    'change' => [DeliveryOrderEventMessage::UPDATE, DeliveryStatusEnum::TRANSFER, ['status'], true],
    'dont change event' => [DeliveryOrderEventMessage::CREATE, DeliveryStatusEnum::TRANSFER, null, false],
    'dont change dirty' => [DeliveryOrderEventMessage::UPDATE, DeliveryStatusEnum::TRANSFER, ['id'], false],
    'dont change status' => [DeliveryOrderEventMessage::UPDATE, DeliveryStatusEnum::ASSEMBLED, ['id'], false],
]);
// endregion
