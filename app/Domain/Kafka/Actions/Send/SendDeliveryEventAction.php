<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\DeliveryPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Orders\Models\Delivery;

class SendDeliveryEventAction extends SendMessageAction
{
    public function execute(Delivery $delivery, string $event)
    {
        $modelEvent = new ModelEventMessage($delivery, new DeliveryPayload($delivery), $event, 'deliveries');
        $this->send($modelEvent);
    }
}
