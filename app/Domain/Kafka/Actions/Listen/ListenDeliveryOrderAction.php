<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\DeliveryOrder\DeliveryOrderEventMessage;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use RdKafka\Message;

class ListenDeliveryOrderAction
{
    public function execute(Message $message)
    {
        $eventMessage = DeliveryOrderEventMessage::makeFromRdKafka($message);
        if ($eventMessage->event == DeliveryOrderEventMessage::UPDATE && in_array('status', $eventMessage->dirty)) {
            /** @var Delivery|null $delivery */
            $delivery = Delivery::query()->find($eventMessage->attributes->delivery_id);
            if (!$delivery) {
                return;
            }

            $delivery->status = $eventMessage->attributes->status;
            if ($delivery->isDirty('status')) {
                $delivery->save();
                DeliveryStatusUpdated::dispatch($delivery);
            }
        }
    }
}
