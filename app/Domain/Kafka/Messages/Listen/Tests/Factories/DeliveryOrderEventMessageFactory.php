<?php

namespace App\Domain\Kafka\Messages\Listen\Tests\Factories;

use App\Domain\Kafka\Messages\Listen\DeliveryOrder\DeliveryOrderEventMessage;
use App\Domain\Orders\Models\Delivery;
use Ensi\TestFactories\Factory;
use RdKafka\Message;

class DeliveryOrderEventMessageFactory extends Factory
{
    protected function definition(): array
    {
        $event = $this->faker->randomElement([
            DeliveryOrderEventMessage::CREATE,
            DeliveryOrderEventMessage::UPDATE,
            DeliveryOrderEventMessage::DELETE,
        ]);

        return [
            'event' => $event,
            'attributes' => [
                'id' => $this->faker->randomNumber(),
                'delivery_id' => Delivery::factory(),
            ],
            'dirty' => $event == DeliveryOrderEventMessage::UPDATE ? $this->faker->randomElements([
                'status',
            ]) : null,
        ];
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }

    protected function mergeDefinitionWithExtra(array $extra): array
    {
        $extraAttributes = $extra['attributes'] ?? [];
        unset($extra['attributes']);
        $array = parent::mergeDefinitionWithExtra($extra);

        $array['attributes'] = array_merge($array['attributes'], $extraAttributes);

        return $array;
    }
}
