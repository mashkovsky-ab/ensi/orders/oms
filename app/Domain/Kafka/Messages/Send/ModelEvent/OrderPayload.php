<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Orders\Models\Order;

class OrderPayload extends Payload
{
    public function __construct(protected Order $order)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->order->id,
            'number' => $this->order->number,

            'responsible_id' => $this->order->responsible_id,

            'customer_id' => $this->order->customer_id,
            'customer_email' => $this->order->customer_email,

            'status' => $this->order->status,
            'status_at' => $this->order->status_at?->toJSON(),

            'cost' => $this->order->cost,
            'price' => $this->order->price,

            'delivery_service' => $this->order->delivery_service,
            'delivery_method' => $this->order->delivery_method,
            'delivery_cost' => $this->order->delivery_cost,
            'delivery_price' => $this->order->delivery_price,
            'delivery_tariff_id' => $this->order->delivery_tariff_id,
            'delivery_point_id' => $this->order->delivery_point_id,
            'delivery_address' => $this->order->delivery_address?->toArray(),
            'delivery_comment' => $this->order->delivery_comment,

            'receiver_name' => $this->order->receiver_name,
            'receiver_phone' => $this->order->receiver_phone,
            'receiver_email' => $this->order->receiver_email,

            'spent_bonus' => $this->order->spent_bonus,
            'added_bonus' => $this->order->added_bonus,
            'promo_code' => $this->order->promo_code,

            'source' => $this->order->source,
            'payment_status' => $this->order->payment_status,
            'payment_status_at' => $this->order->payment_status_at?->toJSON(),
            'payed_at' => $this->order->payed_at?->toJSON(),
            'payment_system' => $this->order->payment_system,
            'payment_method' => $this->order->payment_method,
            'payment_expires_at' => $this->order->payment_expires_at?->toJSON(),
            'payment_link' => $this->order->payment_link,
            'payment_external_id' => $this->order->payment_external_id,

            'is_expired' => $this->order->is_expired,
            'is_expired_at' => $this->order->is_expired_at?->toJSON(),
            'is_return' => $this->order->is_return,
            'is_return_at' => $this->order->is_return_at?->toJSON(),
            'is_partial_return' => $this->order->is_partial_return,
            'is_partial_return_at' => $this->order->is_partial_return_at?->toJSON(),
            'is_problem' => $this->order->is_problem,
            'is_problem_at' => $this->order->is_problem_at?->toJSON(),
            'problem_comment' => $this->order->problem_comment,

            'client_comment' => $this->order->client_comment,

            'created_at' => $this->order->created_at?->toJSON(),
            'updated_at' => $this->order->updated_at?->toJSON(),
        ];
    }
}
