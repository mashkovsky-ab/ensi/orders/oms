<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class DeliveryPayload extends Payload
{
    public function __construct(protected Delivery $delivery)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->delivery->id,
            'order_id' => $this->delivery->order_id,
            'number' => $this->delivery->number,

            'status' => $this->delivery->status,
            'status_at' => $this->delivery->status_at?->toJSON(),

            'cost' => $this->delivery->cost,
            'width' => $this->delivery->width,
            'height' => $this->delivery->height,
            'length' => $this->delivery->length,
            'weight' => $this->delivery->weight,

            'date' => $this->delivery->date?->format(BaseJsonResource::DATE_FORMAT),
            'timeslot' => $this->delivery->timeslot?->toArray(),

            'created_at' => $this->delivery->created_at?->toJSON(),
            'updated_at' => $this->delivery->updated_at?->toJSON(),
        ];
    }
}
