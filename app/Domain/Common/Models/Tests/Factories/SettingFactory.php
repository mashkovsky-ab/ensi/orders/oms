<?php

namespace App\Domain\Common\Models\Tests\Factories;

use App\Domain\Common\Models\Setting;
use Illuminate\Database\Eloquent\Factories\Factory;

class SettingFactory extends Factory
{
    protected $model = Setting::class;

    public function definition()
    {
        return [
            'code' => $this->faker->unique()->text(50),
            'name' => $this->faker->text(50),
            'value' => $this->faker->numerify('##'),
        ];
    }
}
