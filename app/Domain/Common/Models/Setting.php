<?php

namespace App\Domain\Common\Models;

use App\Domain\Common\Models\Tests\Factories\SettingFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id ID параметра
 * @property string $name название параметра
 * @property string $code уникальный код-название параметра
 * @property string $value значение параметра
 *
 * @property Carbon|null $created_at дата создание
 * @property Carbon|null $updated_at дата обновления
 */
class Setting extends Model implements Auditable
{
    use SupportsAudit;

    protected $fillable = [
        'name',
        'value',
    ];

    public static function factory(): SettingFactory
    {
        return SettingFactory::new();
    }

    public static function getValue(string $code): string
    {
        /** @var static $setting */
        $setting = static::query()->where("code", $code)->first();
        if ($setting) {
            return $setting->value;
        } else {
            return match ($code) {
                SettingCodeEnum::PAYMENT => config('common.payment_duration'),
                SettingCodeEnum::PROCESSING => config('common.order_duration'),
                SettingCodeEnum::REFUND => config('common.time_for_refund'),
            };
        }
    }
}
