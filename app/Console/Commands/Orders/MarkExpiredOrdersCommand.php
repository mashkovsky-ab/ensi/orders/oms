<?php

namespace App\Console\Commands\Orders;

use App\Domain\Orders\Actions\Order\MarkExpiredOrdersAction;
use Illuminate\Console\Command;

class MarkExpiredOrdersCommand extends Command
{
    protected $signature = 'orders:mark-expired';
    protected $description = 'Отметить заказы как "Просроченный", при превышении максимального времени обработки заказа';

    public function handle(MarkExpiredOrdersAction $action)
    {
        $action->execute();
    }
}
