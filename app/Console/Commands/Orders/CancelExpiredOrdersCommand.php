<?php

namespace App\Console\Commands\Orders;

use App\Domain\Orders\Actions\Payment\CancelExpiredPaymentsAction;
use Illuminate\Console\Command;

class CancelExpiredOrdersCommand extends Command
{
    protected $signature = 'orders:cancel-expired-payments';
    protected $description = 'Отменить заказы, у которых истёк срок оплаты неоплаченных оплат';

    public function handle(CancelExpiredPaymentsAction $action)
    {
        $action->execute();
    }
}
