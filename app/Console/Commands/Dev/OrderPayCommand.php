<?php

namespace App\Console\Commands\Dev;

use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Carbon\Carbon;
use Illuminate\Console\Command;

class OrderPayCommand extends Command
{
    protected $signature = 'orders:order-pay {orderId}';
    protected $description = 'Команда для тестирования оплаты заказа';

    public function handle()
    {
        if (in_production()) {
            $this->error('Команду нельзя выполнять в боевом окружении');

            return;
        }
        $orderId = $this->argument('orderId');

        /** @var Order $order */
        $order = Order::query()
            ->where('id', $orderId)
            ->first();
        if (!$order) {
            $this->error("Заказ с id={$orderId} не найден");

            return;
        }

        $order->payment_status = PaymentStatusEnum::PAID;
        $order->payed_at = Carbon::now();
        $order->save();
    }
}
