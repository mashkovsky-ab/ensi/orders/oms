<?php

use App\Domain\Common\Models\Setting;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Carbon\Carbon;
use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command orders:cancel-expired-payments success", function () {
    /** @var ComponentTestCase $this */
    /** @var Order $orderCancel */
    $orderCancel = Order::factory()->withPaymentStart()->create([
        'payment_expires_at' => now()->subDay(),
    ]);
    /** @var Order $orderOk */
    $orderOk = Order::factory()->withoutPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    artisan("orders:cancel-expired-payments");

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $orderCancel->id,
        'status' => OrderStatusEnum::CANCELED,
    ]);
    assertDatabaseHas((new Order())->getTable(), [
        'id' => $orderOk->id,
        'status' => OrderStatusEnum::WAIT_PAY,
    ]);
});

test("Command orders:mark-expired success", function () {
    /** @var ComponentTestCase $this */
    $minutes = Setting::getValue(SettingCodeEnum::PROCESSING);

    /** @var Order $orderExpired */
    $orderExpired = Order::factory()->withPaymentStart()->create([
        'created_at' => Carbon::now()->subMinutes($minutes)->subSecond()->format('Y-m-d H:i:s'),
        'status' => OrderStatusEnum::NEW,
        'is_expired' => false,
    ]);
    /** @var Order $orderOk */
    $orderOk = Order::factory()->withoutPaymentStart()->create([
        'created_at' => Carbon::now(),
        'status' => OrderStatusEnum::NEW,
        'is_expired' => false,
    ]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    artisan("orders:mark-expired");

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $orderExpired->id,
        'is_expired' => true,
    ]);
    assertDatabaseHas((new Order())->getTable(), [
        'id' => $orderOk->id,
        'is_expired' => false,
    ]);
});
