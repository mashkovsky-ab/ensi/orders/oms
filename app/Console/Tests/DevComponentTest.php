<?php

use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use function Pest\Laravel\artisan;

use function Pest\Laravel\assertDatabaseHas;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command orders:order-pay {orderId} success", function () {
    /** @var ComponentTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    artisan("orders:order-pay {$order->id}");

    assertDatabaseHas((new Order())->getTable(), [
        'id' => $order->id,
        'payment_status' => PaymentStatusEnum::PAID,
    ]);
});
