<?php

if (!function_exists('in_production')) {

    /**
     * Находится ли приложение в прод режиме
     * @return bool
     */
    function in_production(): bool
    {
        return app()->environment('production');
    }
}
